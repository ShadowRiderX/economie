###################################################################################################
## UNIVERSITÉ RENNES 1 
## Master 1 : Stratégies Digitales et Innovations Numériques
##
## Étude de type Probit de la probabilité d'avoir ou non des Usages Problématiques d'Internet
##
## Auteur : ALEXIS IVORRA
###################################################################################################

# Chargement des librairies
import numpy
import numpy as np
import pandas
import matplotlib.pyplot as plt

csfont = {'fontname': 'Liberation Mono'}
hfont = {'fontname': 'Liberation Mono'}

df_AGE = pandas.read_excel("proba.xls", sheet_name="AGE")

df_AGE_trans = numpy.transpose(df_AGE)
df_AGE_0 = df_AGE_trans[0]
df_AGE_1 = df_AGE_trans[1]

value = ['P(y=1)', 'P(y=2)', 'P(y=3)', 'P(y=4)', 'P(y=5)']

def autolabel(rects):
    for rect in rects:
        height = round(rect.get_height(), 3)
        if height < 0:
            ax_AGE.annotate('{}'.format(height),
                            xy=(rect.get_x() + rect.get_width()/2, height - 0.00055),
                            ha='center', va='center_baseline')
        else:
            ax_AGE.annotate('{}'.format(height),
                            xy=(rect.get_x() + rect.get_width()/2, height + 0.00055),
                            ha='center', va='center_baseline')

fig_a_AGE, ax_AGE = plt.subplots()

x = np.arange(len(value))
width = 0.45
rect1 = ax_AGE.bar(x - width/2, df_AGE_0, width, label='Non Joueurs')
rect2 = ax_AGE.bar(x + width/2, df_AGE_1, width, label='Joueurs')
ax_AGE.set_xticks(x)
ax_AGE.set_xticklabels(value)

ax_AGE.set_xlabel(r"Modalités des UPI", fontsize=10, **hfont)
ax_AGE.set_ylabel(r"EM JV", fontsize=10, **hfont)
ax_AGE.set_title("Effets Marginaux sur la variable AGE", fontsize=15, **csfont)

ax_AGE.grid(True, alpha=0.5)
fig_a_AGE.tight_layout()
plt.legend()
autolabel(rect1)
autolabel(rect2)
plt.show()








