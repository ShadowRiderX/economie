###################################################################################################
## UNIVERSITÉ RENNES 1 
## Master 1 : Stratégies Digitales et Innovations Numériques
##
## Étude de type Probit de la probabilité d'avoir ou non des Usages Problématiques d'Internet
##
## Auteur : ALEXIS IVORRA
###################################################################################################

# Chargement des librairies
import numpy as np
import pandas
import matplotlib.pyplot as plt
import random

# Chargement des données
df_a = pandas.read_excel("data.xls", sheet_name="STATA")
print(df_a.describe())

# Definition des variables
Y = df_a['category_UPI']
x_JV = df_a['JV']
x_ANT = df_a['ANT']
x_FLOW = df_a['FLOW']
x_FOMO = df_a['FOMO']
x_ANXIETE = df_a['ANXIETE']
x_BIENETRE = df_a['BIENETRE']
x_MALETRE = df_a['MALETRE']

value = ['1', '2', '3', '4', '5']


def autolabel(rects):
    for rect in rects:
        height = rect.get_height()
        ax_upi.annotate('{}'.format(height),
                        xy=(rect.get_x() + rect.get_width()/2, height),
                        ha='center', va='bottom')
# GRAPHIQUES DES DISTRIBUTIONS
# Distribution des UPI
A = df_a.groupby(['JV', 'category_UPI'])['category_UPI'].count()
y0 = A[0]
y1 = A[1]
x = np.arange(len(value))
width = 0.35
fig_a_upi, ax_upi = plt.subplots()
rect1 = ax_upi.bar(x - width/2, y0, width, label='Non Joueurs')
rect2 = ax_upi.bar(x + width/2, y1, width, label='Joueurs')
ax_upi.set_xlabel(r"Valeurs des UPI", fontsize=10)
ax_upi.set_xticks(x)
ax_upi.set_xticklabels(value)
ax_upi.set_ylabel(r"Effectifs", fontsize=10)
ax_upi.set_title("Distributions des UPI", fontsize=15)
ax_upi.grid(True, alpha=0.5)
fig_a_upi.tight_layout()
plt.legend()
autolabel(rect1)
autolabel(rect2)
plt.show()



def autolabel(rects):
    for rect in rects:
        height = rect.get_height()
        ax_FLOW.annotate('{}'.format(height),
                        xy=(rect.get_x() + rect.get_width()/2, height),
                        ha='center', va='bottom')
# Distribution des FLOW
FLOW = df_a.groupby(['JV', 'category_FLOW'])['category_FLOW'].count()
y0_FLOW = FLOW[0]
y1_FLOW = FLOW[1]
x_FLOW = np.arange(len(value))
width = 0.35
fig_a_FLOW, ax_FLOW = plt.subplots()
rect1_FLOW = ax_FLOW.bar(x_FLOW - width/2, y0_FLOW, width, label='Non Joueurs')
rect2_FLOW = ax_FLOW.bar(x_FLOW + width/2, y1_FLOW, width, label='Joueurs')
ax_FLOW.set_xlabel(r"Valeurs du FLOW", fontsize=10)
ax_FLOW.set_xticks(x_FLOW)
ax_FLOW.set_xticklabels(value)
ax_FLOW.set_ylabel(r"Effectifs", fontsize=10)
ax_FLOW.set_title("Distributions du FLOW", fontsize=15)
ax_FLOW.grid(True, alpha=0.5)
fig_a_FLOW.tight_layout()
plt.legend()
autolabel(rect1_FLOW)
autolabel(rect2_FLOW)
plt.show()





def autolabel(rects):
    for rect in rects:
        height = rect.get_height()
        ax_ANT.annotate('{}'.format(height),
                        xy=(rect.get_x() + rect.get_width()/2, height),
                        ha='center', va='bottom')
# Distribution des ANT
ANT = df_a.groupby(['JV', 'category_ANT'])['category_ANT'].count()
y0_ANT = ANT[0]
y1_ANT = ANT[1]
x_ANT = np.arange(len(value))
width = 0.35
fig_a_ANT, ax_ANT = plt.subplots()
rect1_ANT = ax_ANT.bar(x_ANT - width/2, y0_ANT, width, label='Non Joueurs')
rect2_ANT = ax_ANT.bar(x_ANT + width/2, y1_ANT, width, label='Joueurs')
ax_ANT.set_xlabel(r"Valeurs des ANTECEDENTS", fontsize=10)
ax_ANT.set_xticks(x_ANT)
ax_ANT.set_xticklabels(value)
ax_ANT.set_ylabel(r"Effectifs", fontsize=10)
ax_ANT.set_title("Distributions des ANTECEDENTS", fontsize=15)
ax_ANT.grid(True, alpha=0.5)
fig_a_ANT.tight_layout()
plt.legend()
autolabel(rect1_ANT)
autolabel(rect2_ANT)
plt.show()





def autolabel(rects):
    for rect in rects:
        height = rect.get_height()
        ax_FOMO.annotate('{}'.format(height),
                        xy=(rect.get_x() + rect.get_width()/2, height),
                        ha='center', va='bottom')
# Distribution des FOMO
FOMO = df_a.groupby(['JV', 'category_FOMO'])['category_FOMO'].count()
y0_FOMO = FOMO[0]
y1_FOMO = FOMO[1]
x_FOMO = np.arange(len(value))
width = 0.35
fig_a_FOMO, ax_FOMO = plt.subplots()
rect1_FOMO = ax_FOMO.bar(x_FOMO - width/2, y0_FOMO, width, label='Non Joueurs')
rect2_FOMO = ax_FOMO.bar(x_FOMO + width/2, y1_FOMO, width, label='Joueurs')
ax_FOMO.set_xlabel(r"Valeurs du FOMO", fontsize=10)
ax_FOMO.set_xticks(x_FOMO)
ax_FOMO.set_xticklabels(value)
ax_FOMO.set_ylabel(r"Effectifs", fontsize=10)
ax_FOMO.set_title("Distributions du FOMO", fontsize=15)
ax_FOMO.grid(True, alpha=0.5)
fig_a_FOMO.tight_layout()
plt.legend()
autolabel(rect1_FOMO)
autolabel(rect2_FOMO)
plt.show()



def autolabel(rects):
    for rect in rects:
        height = rect.get_height()
        ax_ANXIETE.annotate('{}'.format(height),
                        xy=(rect.get_x() + rect.get_width()/2, height),
                        ha='center', va='bottom')
# Distribution des ANXIETE
ANXIETE = df_a.groupby(['JV', 'category_ANXIETE'])['category_ANXIETE'].count()
y0_ANXIETE = ANXIETE[0]
y1_ANXIETE = ANXIETE[1]
x_ANXIETE = np.arange(len(value))
width = 0.35
fig_a_ANXIETE, ax_ANXIETE = plt.subplots()
rect1_ANXIETE = ax_ANXIETE.bar(x_ANXIETE - width/2, y0_ANXIETE, width, label='Non Joueurs')
rect2_ANXIETE = ax_ANXIETE.bar(x_ANXIETE + width/2, y1_ANXIETE, width, label='Joueurs')
ax_ANXIETE.set_xlabel(r"Valeurs de l'ANXIETE", fontsize=10)
ax_ANXIETE.set_xticks(x_ANXIETE)
ax_ANXIETE.set_xticklabels(value)
ax_ANXIETE.set_ylabel(r"Effectifs", fontsize=10)
ax_ANXIETE.set_title("Distributions de l'ANXIETE", fontsize=15)
ax_ANXIETE.grid(True, alpha=0.5)
fig_a_ANXIETE.tight_layout()
plt.legend()
autolabel(rect1_ANXIETE)
autolabel(rect2_ANXIETE)
plt.show()




value2 = ['1', '2', '3', '4']

def autolabel(rects):
    for rect in rects:
        height = rect.get_height()
        ax_BIENETRE.annotate('{}'.format(height),
                        xy=(rect.get_x() + rect.get_width()/2, height),
                        ha='center', va='bottom')
# Distribution des BIENETRE
BIENETRE = df_a.groupby(['JV', 'category_BIENETRE'])['category_BIENETRE'].count()
y0_BIENETRE = BIENETRE[0]
y1_BIENETRE = BIENETRE[1]
x_BIENETRE = np.arange(len(value2))
width = 0.35
fig_a_BIENETRE, ax_BIENETRE = plt.subplots()
rect1_BIENETRE = ax_BIENETRE.bar(x_BIENETRE - width/2, y0_BIENETRE, width, label='Non Joueurs')
rect2_BIENETRE = ax_BIENETRE.bar(x_BIENETRE + width/2, y1_BIENETRE, width, label='Joueurs')
ax_BIENETRE.set_xlabel(r"Valeurs du BIENETRE", fontsize=10)
ax_BIENETRE.set_xticks(x_BIENETRE)
ax_BIENETRE.set_xticklabels(value2)
ax_BIENETRE.set_ylabel(r"Effectifs", fontsize=10)
ax_BIENETRE.set_title("Distributions du BIENETRE", fontsize=15)
ax_BIENETRE.grid(True, alpha=0.5)
fig_a_BIENETRE.tight_layout()
plt.legend()
autolabel(rect1_BIENETRE)
autolabel(rect2_BIENETRE)
plt.show()


def autolabel(rects):
    for rect in rects:
        height = rect.get_height()
        ax_MALETRE.annotate('{}'.format(height),
                        xy=(rect.get_x() + rect.get_width()/2, height),
                        ha='center', va='bottom')
# Distribution des BIENETRE
MALETRE = df_a.groupby(['JV', 'category_BIENETRE'])['category_BIENETRE'].count()
y0_MALETRE = MALETRE[0]
y1_MALETRE = MALETRE[1]
x_MALETRE = np.arange(len(value2))
width = 0.35
fig_a_MALETRE, ax_MALETRE = plt.subplots()
rect1_MALETRE = ax_MALETRE.bar(x_MALETRE - width/2, y0_MALETRE, width, label='Non Joueurs')
rect2_MALETRE = ax_MALETRE.bar(x_MALETRE + width/2, y1_MALETRE, width, label='Joueurs')
ax_MALETRE.set_xlabel(r"Valeurs du MALETRE", fontsize=10)
ax_MALETRE.set_xticks(x_MALETRE)
ax_MALETRE.set_xticklabels(value2)
ax_MALETRE.set_ylabel(r"Effectifs", fontsize=10)
ax_MALETRE.set_title("Distributions du MALETRE", fontsize=15)
ax_MALETRE.grid(True, alpha=0.5)
fig_a_MALETRE.tight_layout()
plt.legend()
autolabel(rect1_MALETRE)
autolabel(rect2_MALETRE)
plt.show()

