###################################################################################################
## UNIVERSITÉ RENNES 1 
## Master 1 : Stratégies Digitales et Innovations Numériques
##
## Étude de type Probit de la probabilité d'avoir ou non des Usages Problématiques d'Internet
##
## Auteur : ALEXIS IVORRA
###################################################################################################

# Chargement des librairies
import numpy
import numpy as np
import pandas
import matplotlib.pyplot as plt

csfont = {'fontname': 'Liberation Mono'}
hfont = {'fontname': 'Liberation Mono'}

df_MALETRE = pandas.read_excel("proba.xls", sheet_name="MALETRE")

df_MALETRE_trans = numpy.transpose(df_MALETRE)
df_MALETRE_0 = df_MALETRE_trans[0]
df_MALETRE_1 = df_MALETRE_trans[1]

value = ['P(y=1)', 'P(y=2)', 'P(y=3)', 'P(y=4)', 'P(y=5)']

def autolabel(rects):
    for rect in rects:
        height = round(rect.get_height(), 3)
        if height < 0:
            ax_MALETRE.annotate('{}'.format(height),
                            xy=(rect.get_x() + rect.get_width()/2, height - 0.005),
                            ha='center', va='center_baseline')
        else:
            ax_MALETRE.annotate('{}'.format(height),
                            xy=(rect.get_x() + rect.get_width()/2, height + 0.005),
                            ha='center', va='center_baseline')

fig_a_MALETRE, ax_MALETRE = plt.subplots()

x = np.arange(len(value))
width = 0.45
rect1 = ax_MALETRE.bar(x - width/2, df_MALETRE_0, width, label='Non Joueurs')
rect2 = ax_MALETRE.bar(x + width/2, df_MALETRE_1, width, label='Joueurs')
ax_MALETRE.set_xticks(x)
ax_MALETRE.set_xticklabels(value)

ax_MALETRE.set_xlabel(r"Modalités des UPI", fontsize=10, **hfont)
ax_MALETRE.set_ylabel(r"EM JV", fontsize=10, **hfont)
ax_MALETRE.set_title("Effets Marginaux sur la variable MALETRE", fontsize=15, **csfont)

ax_MALETRE.grid(True, alpha=0.5)
fig_a_MALETRE.tight_layout()
plt.legend()
autolabel(rect1)
autolabel(rect2)
plt.show()








