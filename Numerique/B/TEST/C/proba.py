# Chargement des librairies
import numpy
import numpy as np
import pandas
import matplotlib.pyplot as plt

csfont = {'fontname': 'Liberation Mono'}
hfont = {'fontname': 'Liberation Mono'}

df_JV = pandas.read_excel("proba.xls", sheet_name="JV2")
df_AGE = pandas.read_excel("proba.xls", sheet_name="AGE")
df_MALETRE = pandas.read_excel("proba.xls", sheet_name="MALETRE")

value = ['', 'P(y=1)', '', 'P(y=2)', '', 'P(y=3)', '', 'P(y=4)', '', 'P(y=5)']
df_JV_trans = numpy.transpose(df_JV)
df_JV_0 = df_JV_trans[0]
df_JV_1 = df_JV_trans[1]

fig_a_JV, ax_JV = plt.subplots()
ax_JV.plot(df_JV_0, ls='--', color='#1f77b4', alpha=0.7, label='Non Joueurs')
ax_JV.plot(df_JV_1, ls='--', color='#ff7f0e', alpha=0.7, label='Joueurs')
ax_JV.set_xlabel(r"Modalités des UPI", fontsize=10, **hfont)
ax_JV.set_xticklabels(value)
ax_JV.set_ylabel(r"EM JV", fontsize=10, **hfont)
ax_JV.set_title("Effets Marginaux sur la variable JV", fontsize=15, **csfont)
ax_JV.grid(True)
fig_a_JV.tight_layout()
plt.legend()
plt.show()



value2 = ['P(y=1)', 'P(y=2)', 'P(y=3)', 'P(y=4)', 'P(y=5)']

def autolabel(rects):
    for rect in rects:
        height = rect.get_height()
        ax2_JV.annotate('{}'.format(height),
                        xy=(rect.get_x() + rect.get_width()/2, height),
                        ha='center', va='bottom')

x = np.arange(len(value2))
width = 0.35
fig2_a_JV, ax2_JV = plt.subplots()
rect1 = ax2_JV.bar(x - width/2, df_JV_0, width, label='Non Joueurs')
rect2 = ax2_JV.bar(x + width/2, df_JV_1, width, label='Joueurs')
ax2_JV.set_xlabel(r"Valeurs des UPI", fontsize=10)
ax2_JV.set_xticks(x)
ax2_JV.set_xticklabels(value2)
ax2_JV.set_ylabel(r"Effectifs", fontsize=10)
ax2_JV.set_title("Distributions des UPI", fontsize=15)
ax2_JV.grid(True, alpha=0.5)
fig2_a_JV.tight_layout()
plt.legend()
autolabel(rect1)
autolabel(rect2)
plt.show()
