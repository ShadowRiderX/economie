# Chargement des librairies
import numpy
import numpy as np
import pandas
import matplotlib.pyplot as plt

csfont = {'fontname': 'Liberation Mono'}
hfont = {'fontname': 'Liberation Mono'}

df_JV = pandas.read_excel("proba.xls", sheet_name="JV")

df_JV_trans = numpy.transpose(df_JV)
df_JV_0 = df_JV_trans[0]
df_JV_1 = df_JV_trans[1]

value = ['P(y=1)', 'P(y=2)', 'P(y=3)', 'P(y=4)', 'P(y=5)']

def autolabel(rects):
    for rect in rects:
        height = round(rect.get_height(), 3)
        if height < 0:
            ax_JV.annotate('{}'.format(height),
                           xy=(rect.get_x() + rect.get_width() / 2, height - 0.003),
                           ha='center', va='center_baseline')
        else:
            ax_JV.annotate('{}'.format(height),
                           xy=(rect.get_x() + rect.get_width() / 2, height + 0.003),
                           ha='center', va='center_baseline')

fig_a_JV, ax_JV = plt.subplots()

x = np.arange(len(value))
width = 0.45
rect1 = ax_JV.bar(x - width/2, df_JV_0, width, label='Non Joueurs')
rect2 = ax_JV.bar(x + width/2, df_JV_1, width, label='Joueurs')
ax_JV.set_xticks(x)
ax_JV.set_xticklabels(value)

ax_JV.plot(x, df_JV_0, ls='--', color='#1f77b4', alpha=0.7)
ax_JV.plot(x, df_JV_1, ls='--', color='#ff7f0e', alpha=0.7)

ax_JV.set_xlabel(r"Modalités des UPI", fontsize=10, **hfont)
ax_JV.set_ylabel(r"EM JV", fontsize=10, **hfont)
ax_JV.set_title("Effets Marginaux sur la variable JV", fontsize=15, **csfont)

ax_JV.grid(True, alpha=0.5)
fig_a_JV.tight_layout()
plt.legend()
autolabel(rect1)
autolabel(rect2)
plt.show()

