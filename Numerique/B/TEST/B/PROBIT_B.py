# Chargement des librairies
import pandas
import statsmodels as sm
from statsmodels.tools import add_constant
from statsmodels.api import Logit
from statsmodels.api import Probit
import numpy
import matplotlib.pyplot as plt
import scipy


# Chargement de la base donnée
DF_ACHATSJV = pandas.read_excel("ACHATSJV.xls", sheet_name="joueurs")

# Informations sur les variables
print(DF_ACHATSJV.info())

# Y variable endogène ACHATS jeux vidéos est binaire OUI=1;NON=0 et est la derniere colone
Y_ACHATS = DF_ACHATSJV.iloc[:, -1]

# X les variables exogènes sont les variables qui précèdent la dernière colone
X_ACHATS = DF_ACHATSJV.iloc[:, :-1]

# Comptage des modalités de Y_ACHATS
print(Y_ACHATS.value_counts())

# Données X avec la constante
XX_ACHATS_Bis = sm.tools.add_constant(X_ACHATS)

# Vérifier la structure
print(XX_ACHATS_Bis.info())

# Visualisation des premières lignes de la structure
# Premières lignes
print(XX_ACHATS_Bis.head())

# Régression logistique - on passe la cible et les explicatives
lr = Probit(endog=Y_ACHATS, exog=XX_ACHATS_Bis)

# Lancer les calculs
# Algorithme de Newton-Raphson utilisé par défaut
#https://www.statsmodels.org/stable/generated/statsmodels.discrete.discrete_model.Logit.fit.html
res = lr.fit()

# Résumé des résultats
print(res.summary())

# Intervalle de confiance des coefficients à 90%
print(res.conf_int(alpha=0.1))

# Valeurs estimées par la régression en resubstitution
print(res.fittedvalues)

# Voici les coefficients estimés
print(res.params)

# Voici la description du premier individu
print(XX_ACHATS_Bis.iloc[0, :])

# Et si on fait le produit scalaire – valeur du Probit pour l’individu n°0
print(numpy.sum(res.params*XX_ACHATS_Bis.iloc[0, :]))

# La règle d'affectation consiste à confronter le LOGIT à la valeur seuil 0
predResub = numpy.where(res.fittedvalues > 0, 1, 0)
print(predResub)

# On peut en déduire la matrice de confusion
print(pandas.crosstab(Y_ACHATS, predResub))

# Matrice de confusion en resubstitution directement fournie par l'outil
print(res.pred_table())


#accès à la log-vraisemblance du modèle
print("Log-vraisemblance du modèle : %.4f" % (res.llf))

#log-vraisemblance du null modèle
print("Log-vraisemblance du null modèle : %.4f" % (res.llnull))

# R2 de McFadden
R2MF = 1 - res.llf / res.llnull
print(R2MF)

# Evaluation basée sur les scores
# Scores fournis par la régression pour les 13 premier indiv
scores = lr.cdf(res.fittedvalues)
print(scores[:13])

# Data frame temporaire avec y et les scores
df = pandas.DataFrame({"y": Y_ACHATS, "score": scores})

# 5 intervalles de largeur égales
intv = pandas.cut(df.score, bins=5, include_lowest=True)

# Intégrées dans le df
df['intv'] = intv
print(df)

# Moyenne des scores par groupe
m_score = df.pivot_table(index="intv", values="score", aggfunc="mean")
print(m_score)

# Moyenne des y - qui équivaut à une proportion puisque 0/1
m_y = df.pivot_table(index="intv", values="y", aggfunc="mean")
print(m_y)

#construire la diagonale
plt.plot(numpy.arange(0, 1, 0.1), numpy.arange(0, 1, 0.1), 'b')

#rajouter notre diagramme
plt.plot(m_score, m_y, "go-")

#titre
plt.title("Diagramme de fiabilité")

#faire apparaître
plt.show()

# Test de Hosmer-Lemeshow
#data frame temporaire avec y et les scores
df = pandas.DataFrame({"y": Y_ACHATS, "score": scores})

# 10 intervalles de fréquences égales
intv = pandas.qcut(df.score, q=10)

# Intégrées dans le df
df['intv'] = intv
print(df)

# Effectifs par groupe
n_tot = df.pivot_table(index="intv", values="y", aggfunc="count").values[:, 0]
print(n_tot)

# Somme des scores par groupes
s_scores = df.pivot_table(index='intv', values="score", aggfunc="sum").values[:, 0]
print(s_scores)

# Nombre de positifs par groupe
n_pos = df.pivot_table(index="intv", values="y", aggfunc="sum").values[:, 0]
print(n_pos)

# Nombre de négatifs par groupe
n_neg = n_tot - n_pos
print(n_neg)

C1 = numpy.sum((n_pos - s_scores)**2/s_scores)
print(C1)

C2 = numpy.sum((n_neg - (n_tot - s_scores))**2/((n_tot - s_scores)))
print(C2)

# Statistique de Hosmer-Lemeshow
HL = C1 + C2
print(HL)

# Probabilité critique
pvalue = 1.0 - scipy.stats.chi2.cdf(HL, 11)
print(pvalue)

# Test de significativité globale de la régression
# Via le test du rapport de vraisemblance
# Déviance du modèle
dev_modele = (-2) * res.llf
print("Deviance du modèle : %.4f " % (dev_modele))

# Déviance du modèle trivial
dev_null = (-2) * res.llnull
print("Deviance du modèle : %.4f " % (dev_null))

# Statistique du rapport de vraisemblance
LR_stat = dev_null - dev_modele
print("Stat. du rapport de vraisemblance : %.4f " % (LR_stat))

# Laquelle était fournie directement par l'objet
print("Stat. du rapport de vraisemblance via l'objet résultat : %.4f" % (res.llr))

# Degré de liberté du test (nb. de coef. estimés excepté la constante)
print(res.df_model)

# p-value du test
pvalue = 1.0 - scipy.stats.chi2.cdf(res.llr, res.df_model)
print(pvalue)

# Laquelle était également fournie par l'objet
print(res.llr_pvalue)


#AIC du modèle
print("AIC du modèle : %.4f" % (res.aic))

#AIC du modèle trivial - 1 seul param. estimé, la constante
aic_null = (-2) * res.llnull + 2 * (1)
print("AIC du modèle trivial : %.4f" % (aic_null))

if res.aic < aic_null:
    print("Le modèle est globalement pertinent")
else:
    print("Le modèle n'est pas pertinent")

#BIC du modèle
print("BIC du modèle : %.4f" % (res.bic))

# BIC du modèle trivial - 1 seul param. estimé, la constante
n = 14
bic_null = (-2) * res.llnull + numpy.log(n) * (1)
print("BIC du modèle trivial : %.4f" % (bic_null))

if res.bic < bic_null:
    print("Le modèle est globalement pertinent")
else:
    print("Le modèle n'est pas pertinent")