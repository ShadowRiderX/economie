****
## UNIVERSITÉ RENNES 1  
## Master 1 : Stratégies Digitales et Innovations Numériques  

## Etude sur les usages problématique d’internet. 
****


>La base de donnée n'étant pas libre, elle ne peut pas etre fourni ici !  

# PARTIE 2 : 
>Étude de type Probit de la probabilité d'avoir ou non des Usages Problématiques d'Internet  

## Descriptifs des variables

|   Variables  |                                    Descriptions                                    |
|:------------ |:---------------------------------------------------------------------------------- |
| category_UPI | 1 si les individus n'ont jamais d'UPI                                              |
|              | 2 si les individus ont Rarement d’UPI                                              |
|              | 3 si les individus ont Parfois des UPI                                             |
|              | 4 si les individus ont Souvent des UPI                                             |
|              | 5 si les individus ont Très Souvent des UPI                                        |
| JV           | 1 si les individus sont des Joueurs                                                |
|              | 0 si les individus ne sont pas des Joueurs                                         |
| FEMME        | 1 si les individus sont des femmes                                                 |
|              | 0 si les individus sont des hommes                                                 |
| AGE          | Aucun                                                                              |
| moins_2000   | 1 si les individus ont moins de 2000 € de revenu et 0 sinon                        |
| moins_bac    | 1 si les individus ont un niveau de diplôme inférieur au bac et 0 sinon            |
| FLOW (1)     | Aucun                                                                              |
| ANT (2)      | Aucun                                                                              |
| FOMO (3)     | Aucun                                                                              |
| ANXIETE (4)  | Aucun                                                                              |
| BIENETRE (5) | Aucun                                                                              |
| MALETRE (6)  | Aucun                                                                              |    

### Définitions des variables psychologiques :
* Le flow (1) est un état mental atteint par un individu lorsqu'il est complètement plongé dans une activité et qu'il se trouve dans un état maximal de concentration, de plein engagement et de satisfaction.  
* Les antécédents (2) sont liés à une bonne représentation et maitrise de soi et de ses compétences.  
* Le fomo (3) ou anxiété de ratage est une sorte d'anxiété sociale caractérisée par la peur constante de manquer une nouvelle importante ou un autre événement quelconque donnant une occasion d'interagir socialement.  
* L’anxiété (4) est un état psychologique et physiologique en lien avec le sentiment de peur, d’inquiétude et de crainte.  
* Le bien-être (5) est le sentiment qu’éprouve un individu en l’absence de problème. Il est lié à la satisfaction de l’individu et à une bonne estime de soi.
* Le mal-être (6) est, à la différence du bien-être, le sentiment qu’éprouve un individu quand celui-ci a des problèmes dans sa vie.  




