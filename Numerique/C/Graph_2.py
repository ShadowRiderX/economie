# Chargement des librairies
import numpy as np
import pandas
import matplotlib.pyplot as plt

VARIABLE_I = "FANTASME"
TITRE_GRAPH = "Nombre d'individus ayant du fantasme ou non"

# Chargement des données
data = pandas.read_excel("DATABASE.xls", sheet_name=VARIABLE_I)
print(data.describe())

total_height = data.count()

# Graphiques
for variable in data:
    print(variable)
    A = data.groupby([VARIABLE_I, variable])[variable].count()
    print(A)
    fig, ax = plt.subplots()

    X = np.arange(len(A[0].index))
    LABEL = A[0].index

    bars1 = ax.bar(X - 0.20, A[0], width=0.20, color='r', alpha=0.7, label='Non')
    bars2 = ax.bar(X + 0.20, A[1], width=0.20, color='g', alpha=0.7, label='Oui')

    for bar in bars1:
        height = bar.get_height()
        heights = round((height / total_height[variable]) * 100, 2)
        ax.text(bar.get_x() + bar.get_width() / 2, height, f"{heights} %" + "\n" + f"({height})", ha='center', va='bottom')

    for bar in bars2:
        height = bar.get_height()
        heights = round((height / total_height[variable]) * 100, 2)
        ax.text(bar.get_x() + bar.get_width() / 2, height, f"{heights} %" + "\n" + f"({height})", ha='center', va='bottom')

    ax.set_xlabel(fr"{variable}", fontsize=10)
    ax.set_ylabel(r"Effectifs", fontsize=10)
    ax.set_title(TITRE_GRAPH, fontsize=15)
    ax.grid(True)
    fig.tight_layout()
    plt.xticks(X, LABEL)
    plt.legend()
    plt.show()

