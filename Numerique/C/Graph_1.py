import pandas
import matplotlib.pyplot as plt


DATA = pandas.read_excel("DATABASE.xls", sheet_name="DATA")
print(DATA.isnull().sum().sort_values(ascending=False))

DATA.fillna("NaN", inplace=True)

LVARIABLES = DATA.columns.tolist()

total_height = DATA.count()

# Fonction Graphique
def create_graph(DATA, x_column, y_column, x_label, y_label, title):
    grouped = DATA.groupby([x_column])[y_column].count()
    print(grouped)
    x_values = sorted(list(set(DATA[x_column])))
    fig, ax = plt.subplots()
    tick_labels = range(0, len(x_values))

    i = 0
    for cat in x_values:
        bars = ax.bar(x_values[i], grouped[i], alpha=0.7, label=cat)
        for bar in bars:
            height = bar.get_height()
            heights = round((height / total_height[i])*100, 2)
            ax.text(bar.get_x() + bar.get_width() / 2, height, f"{heights} %" + " | " + f"{height}", ha='center', va='bottom')
        i += 1
    ax.set_xlabel(x_label, fontsize=10)
    ax.set_ylabel(y_label, fontsize=10)
    ax.set_title(title, fontsize=15)
    ax.set_xticklabels(labels=tick_labels)
    ax.grid(True)
    fig.tight_layout()
    plt.legend(bbox_to_anchor=(0.5, -0.1), loc='upper center', ncol=3)
    plt.subplots_adjust(bottom=0.2)
    # plt.xticks(rotation=25, ha='right')
    plt.show()


for Variables in LVARIABLES[:-3]:
    print(Variables)
    create_graph(DATA, Variables, Variables, "Nombre d'individu", "Effectifs", f"Barplot des effectifs {Variables}")



