#######################################################################
## ACM                                                               ##
#######################################################################

# Sources Youtube : https://www.youtube.com/watch?v=oY0mtzcvRYk
# Sources Livre Ricco Rakotomalala : Pratique des Méthodes Factorielles avec Python

import pandas
import numpy
from fanalysis.mca import MCA
import matplotlib.pyplot as plt


ind_actifs = pandas.read_excel("DATABASE.xls", sheet_name="DATA")
ind_actifs.fillna("NaN", inplace=True)

print(ind_actifs.info())
print(ind_actifs.describe())

print(ind_actifs["NB_INDIV_FOY"].mean())
print(ind_actifs["NB_PEUR"].mean())
print(ind_actifs["NB_VICTIME"].mean())

## Isoler les variables actives
X_actifs = ind_actifs[ind_actifs.columns[:6]]
print(X_actifs.columns)

## Nombre d'observations
n = X_actifs.shape[0]
print(n)

## Nombre de variables
p = X_actifs.shape[1]
print(p)

## Nombre total de modalités
M = numpy.sum(X_actifs.apply(axis=0, func=lambda x: x.value_counts().shape[0]))
print(M)

## Nombre max de facteurs (Livre page 293)
print(M-p)

## Inertie total (Livre page 299)
print(M/p-1)

###################################################################################
# ACM avec fanalysis
###################################################################################

## Intanciation
acm = MCA(row_labels=X_actifs.index, var_labels=X_actifs.columns)

## Execution
acm.fit(X_actifs.values)

## Valeurs propres
print(acm.eig_)

print("***********************************************************************")
A = round(acm.eig_[1][0], 2)
B = round(acm.eig_[1][1], 2)
print(f"Contibutions {A}")
print(f"Contibutions {B}")
print("***********************************************************************")

# Inertie totale = somme des valeurs propres
print(numpy.sum(acm.eig_[0]))

# Graphiques des valeurs propres
acm.plot_eigenvalues()

## Pourcentage de variance cumulée
acm.plot_eigenvalues("cumulative")

# Informations sur les individus
info_lig = acm.row_topandas()
print(info_lig.columns)

# Nuage de point - premier plan factoriel
"""acm.mapping_row(num_x_axis=1, num_y_axis=2)"""


###################################################################################
# Information sur les points de modalités
###################################################################################

# Coordonnées et Info
info_col = acm.col_topandas()
print(info_col.columns)

# Coordonées dans le premier plan factoriel
coord_col = info_col[["col_coord_dim1", "col_coord_dim2"]]
print(coord_col)

# MASS - poids relatifs des modalitées (Livre page 297)
# Qui constitue aussi le profile moyen (Livre page 290)
col_mass = acm.c_/(n*p)
print(col_mass)

# Facteurs sont centré : verification moyenne pondérée des modalités
print(coord_col.apply(axis=0, func=lambda x: numpy.sum(col_mass[0]*x)))

# Dispertion = valeurs propres => oui (Livre page 298)
print(coord_col.apply(axis=0, func=lambda x: numpy.sum(col_mass[0]**x)))

# Carte des modalités
acm.mapping_col(num_x_axis=1, num_y_axis=2, short_labels=False)

print("***********************************************************************")
A = round(acm.eig_[1][0], 2)
B = round(acm.eig_[1][1], 2)
print(f"dim1 {A}")
print(f"dim2 {B}")
print("***********************************************************************")

# Autre graphique avec modification des dimentions du tableau
fig, ax = plt.subplots(figsize=(10, 10))
ax.axis([-25, +25, -25, +25])
ax.plot([-25, +25], [0, 0], color="silver", linestyle="--")
ax.plot([0, 0], [-25, +25], color="silver", linestyle="--")
ax.set_xlabel(f"Dim1 ({A} %)")
ax.set_ylabel(f"Dim2 ({B} %)")
plt.title("Carte des points de modalités")
for i in range(coord_col.shape[0]):
    ax.text(coord_col.iloc[i, 0], coord_col.iloc[i, 1], coord_col.index[i])

plt.show()

# Contributions au premier facteur : nb_values=7
acm.plot_col_contrib(num_axis=1, short_labels=False, figsize=(5, 3))

# Contributions au second facteur
acm.plot_col_contrib(num_axis=2, short_labels=False, figsize=(5, 3))

# Représentation simultanée : Individus et Modalités
"""acm.mapping(num_x_axis=1, num_y_axis=2)"""



###################################################################################
# Traitement des variables illustratives supplémentaire : Variables Qualitatives
###################################################################################

X_illus_quali = ind_actifs[ind_actifs.columns[6:39]]
print(X_illus_quali.columns)

for var_illus in X_illus_quali.columns:
    print(var_illus)

    # Positionnement des variables supplémentaire
    coord = info_lig[["row_coord_dim1", "row_coord_dim2"]].copy()
    coord[var_illus] = X_illus_quali[var_illus]

    # Moyenne conditionnelles - Livre page 341
    coord_fact = pandas.pivot_table(data=coord, values=["row_coord_dim1", "row_coord_dim2"], index=f"{var_illus}", aggfunc="mean")

    # Corrigés par la racine carrées des valeurs propres
    coord_fact = coord_fact/numpy.sqrt(acm.eig_[0][:2])
    print(coord_fact)

    # Place les modalités de la variables PCS dans le plan factoriel
    fig, ax = plt.subplots(figsize=(10, 10))
    ax.axis([-25, +25, -25, +25])
    ax.plot([-25, +25], [0, 0], color="silver", linestyle="--")
    ax.plot([0, 0], [-25, +25], color="silver", linestyle="--")
    ax.set_xlabel(f"Dim1 ({A} %)")
    ax.set_ylabel(f"Dim2 ({B} %)")
    plt.title(f"Carte des modalités + {var_illus}")
    for i in range(coord_col.shape[0]):
        ax.text(coord_col.iloc[i, 0], coord_col.iloc[i, 1], coord_col.index[i], color="grey")

    for j in range(coord_fact.shape[0]):
        ax.text(coord_fact.iloc[j, 0], coord_fact.iloc[j, 1], coord_fact.index[j], color="blue")
    plt.show()





