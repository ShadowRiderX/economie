# Librairie
import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
import statsmodels.api as sm


# Charger les données
data = pd.read_excel("DATABASE.xls", sheet_name='MODEL_1')
print(data.info())


print("\n**************************************************************************\n")

print("Mode des variables")
for col in data.columns:
    print(col)
    modes = data[col].mode()
    print(modes)

print("\n**************************************************************************\n")

# Description
def frequences(D):
    for v in D.columns:
        if (D[v].dtype == 'object'):
            print(D[v].value_counts())
            print('----')

# Vérification des modalités
print(frequences(data))

# Recodage, sauf la cible
Xdata = pd.get_dummies(data[data.columns[:-1]], drop_first=True)
print(Xdata.info())

# Premières valeurs
print(Xdata.head())

# Modélisation
clf = LogisticRegression(solver='newton-cg')
clf.fit(Xdata, data['PEUR_INFO_PERSO'])

# Coefficients
print(clf.coef_)

# Les classes
print(clf.classes_)

# Summary simple
print(pd.DataFrame(np.transpose(clf.coef_), index=Xdata.columns, columns=['coefficients']))

# Summary totale
yyy = np.asarray(data['PEUR_INFO_PERSO'])
xxx = np.asarray(Xdata)

# on ajoute une colonne pour la constante
x_stat = sm.add_constant(xxx)

# on ajuste le modèle
model = sm.Logit(yyy, x_stat)
result = model.fit()

varr = list(Xdata.columns.insert(0, 'const'))

print(result.summary(xname=varr))



