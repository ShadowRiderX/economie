import pandas as pd
import statsmodels.formula.api as sm


# Charger les données
data = pd.read_excel("DATABASE.xls", sheet_name='MODEL_2')

regression = sm.probit(formula='PEUR_INFO_PERSO ~ SEXE + TAGE + ETUDE', data=data).fit()
print(regression.get_margeff(at='overall', method='dydx').summary())



print("\n*******************************************************************\n")



from pandas.api.types import CategoricalDtype
from statsmodels.miscmodels.ordinal_model import OrderedModel

data = data.fillna(data.mode().iloc[0])

data["PEUR_INFO_PERSO"] = data["PEUR_INFO_PERSO"].astype(str)

modalites = data['PEUR_INFO_PERSO'].unique()

## Transformation du type objet de la variable en type ordered
cat_type = CategoricalDtype(categories=['0', '1'], ordered=True)
data["PEUR_INFO_PERSO"] = data["PEUR_INFO_PERSO"].astype(cat_type)


## Création du modèle probit
mod_prob = OrderedModel.from_formula("PEUR_INFO_PERSO ~ 0 + SEXE + TAGE + ETUDE", data, distr='probit', hasconst=False)
res_prob = mod_prob.fit(method='bfgs', disp=False, cov_type="hac", cov_kwds={"maxlags": 2})
print(res_prob.summary())



print("\n*******************************************************************\n")



import statsmodels.formula.api as sm


data = data.astype(float)

dummy_category_UPI = pd.get_dummies(data['PEUR_INFO_PERSO'], prefix='PEUR_INFO_PERSO')
dummy_category_UPI = dummy_category_UPI.rename(columns={
    "PEUR_INFO_PERSO_0.0": "PEUR_INFO_PERSO_0",
    "PEUR_INFO_PERSO_1.0": "PEUR_INFO_PERSO_1"
})

data_t = pd.concat([data, dummy_category_UPI], axis=1)



print("\n*******************************************************************\n")

regression = sm.probit(formula="PEUR_INFO_PERSO_0 ~ SEXE + TAGE + ETUDE", data=data_t).fit()
print(regression.get_margeff(at='overall', method='dydx').summary())

print("\n")

regression = sm.probit(formula="PEUR_INFO_PERSO_1 ~ SEXE + TAGE + ETUDE", data=data_t).fit()
print(regression.get_margeff(at='overall', method='dydx').summary())



print("\n*******************************************************************\n")



data2 = pd.read_excel("DATABASE.xls", sheet_name='MODEL_1')
data_t2 = pd.concat([data2, dummy_category_UPI], axis=1)

regression = sm.probit(formula="PEUR_INFO_PERSO_0 ~ SEXE + TAGE + ETUDE", data=data_t2).fit()
print(regression.get_margeff(at='overall', method='dydx').summary())

print("\n")

regression = sm.probit(formula="PEUR_INFO_PERSO_1 ~ SEXE + TAGE + ETUDE", data=data_t2).fit()
print(regression.get_margeff(at='overall', method='dydx').summary())

