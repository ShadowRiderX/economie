#######################################################################
## CAH                                                               ##
#######################################################################

import pandas
from matplotlib import pyplot as plt
import seaborn as sns
from scipy.cluster.hierarchy import dendrogram, linkage, fcluster
import numpy as np
from sklearn import cluster
from sklearn import metrics
from sklearn import preprocessing
from sklearn.decomposition import PCA


# Données
DATA = pandas.read_excel("DATABASE.xls", sheet_name="CAH", index_col=0)
print(DATA.info())
print(DATA)

# Dimension des données
print(DATA.shape)

# Statistiques descriptives
print(DATA.describe())

# Corrélations
correlation = DATA.corr(method="pearson")
sns.heatmap(correlation, annot=True, cmap="coolwarm")
plt.title('Tableau de corrélation entre les variables')
plt.show()

# Pairplot
graph = sns.pairplot(DATA)
graph.fig.suptitle("Graphique des pairplot")
plt.show()

#######################################################################
# CAH
print("CAH")
# Générer la matrice des liens
Z = linkage(DATA, method='ward', metric='euclidean')

# Affichage du dendrogramme
plt.title("CAH")
dendrogram(Z, labels=DATA.index, orientation='right', color_threshold=0)
plt.show()

# Matérialisation des classes (hauteur t = 1)
plt.title('CAH avec matérialisation de 6 groupes avec 3 clusters distincts')
dendrogram(Z, labels=DATA.index, orientation='right', color_threshold=1)
plt.show()

# Découpage à la hauteur t = 1
groupes_cah = fcluster(Z, t=1, criterion='distance')
print(groupes_cah)

print("\n")

# Index triés des groupes
idg = np.argsort(groupes_cah)

# Affichage des observations et leurs groupes
print(pandas.DataFrame(DATA.index[idg], groupes_cah[idg]))

print("\n")
print("\n")

#######################################################################
# Méthode des K-mean
# k-means sur les données centrées et réduites
print("Méthode des K-mean")
print("\n")

kmeans = cluster.KMeans(n_clusters=3, n_init='auto', random_state=42)
kmeans.fit(DATA)

# Index triés des groupes
idk = np.argsort(kmeans.labels_)

# Affichage des observations et leurs groupes
print("Affichage des observations et leurs groupes")
print(pandas.DataFrame(DATA.index[idk], kmeans.labels_[idk]))
print("\n")

# Distances aux centres de classes des observations
print("Distances aux centres de classes des observations")
print(kmeans.transform(DATA))
print("\n")

# Correspondance avec les groupes de la CAH
print("correspondance avec les groupes de la CAH")
correspond = pandas.crosstab(groupes_cah, kmeans.labels_)
print(correspond)
print("\n")

# Utilisation de la métrique "silhouette"
res = np.arange(9, dtype="double")
for k in np.arange(9):
    km = cluster.KMeans(n_clusters=k+2, n_init='auto', random_state=42)
    km.fit(DATA)
    res[k] = metrics.silhouette_score(DATA, km.labels_)
print(res)

# Graphique
plt.title("Silhouette")
plt.xlabel("Groupes")
plt.plot(np.arange(2, 11, 1), res)
plt.show()

#######################################################################
# Interprétation des classes
# Moyenne par variable
m = DATA.mean()

# TSS
print("Somme des carrés totale")
TSS = DATA.shape[0]*DATA.var(ddof=0)
print(TSS)

# data.frame conditionnellement aux groupes
gb = DATA.groupby(kmeans.labels_)

# Effectifs conditionnels
print("Effectifs conditionnels")
nk = gb.size()
print(nk)

# Moyennes conditionnelles
print("Moyennes conditionnelles")
mk = gb.mean()
print(mk)

# Pour chaque groupe écart à la moyenne par variable
EMk = (mk-m)**2

# Pondéré par les effectifs du groupe
EM = EMk.multiply(nk, axis=0)

# Somme des valeurs => BSS
print("Somme des valeurs")
BSS = np.sum(EM, axis=0)
print(BSS)

# Carré du rapport de corrélation
# Variance expliquée par l'appartenance aux groupes pour chaque variable
R2 = BSS/TSS
print("R² : ", R2)

#######################################################################
# ACP
acp = PCA(n_components=2).fit_transform(DATA)
# Projeter dans le plan factoriel avec un code couleur différent selon le groupe
for couleur, k in zip(['red', 'blue', 'lawngreen', 'aqua', 'pink'], [0, 1, 2, 3, 4]):
    plt.scatter(acp[kmeans.labels_ == k, 0], acp[kmeans.labels_ == k, 1], c=couleur)
plt.title("Analyse en Composante Principale")
plt.show()

# ACP
acp_subset = PCA(n_components=2).fit_transform(DATA)
# Projeter dans le plan factoriel avec un code couleur selon le groupe
plt.figure(figsize=(10, 10))
for couleur, k in zip(['red', 'blue', 'lawngreen', 'aqua', 'pink'], [0, 1, 2, 3, 4]):
    plt.scatter(acp_subset[kmeans.labels_ == k, 0], acp_subset[kmeans.labels_ == k, 1], c=couleur)

for i, label in enumerate(DATA.index):
    plt.annotate(label, (acp_subset[i, 0], acp_subset[i, 1]),
                 xytext=(acp_subset[i, 0], acp_subset[i, 1]))
plt.plot([-3, 15.5], [0, 0], 'k--')
plt.plot([0, 0], [-2, 3], 'k--')
plt.title("ACP")
plt.show()

