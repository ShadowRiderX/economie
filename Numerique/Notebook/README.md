# Dashboard jupyter notebook

Le fichier index.html permet d'ouvrir un dashboard rassemblant différents notebook jupyter.
Les pages html généré avec jupyter sont appeler dans index.html avec une balise iframe.


# Bouton Cacher/Afficher les codes dans les notebook

Ajouter aux pages html jupyter notebook en dessous de la balise body :

```code
<body class="jp-Notebook" data-jp-theme-light="true" data-jp-theme-name="JupyterLab Light">
```

ce code-ci :
```code
<div id="menu" style="margin-left:-15px; position: fixed; z-index: 9999;">
    <button id="toggleCode">Cacher Code</button>
</div>

<script type="text/javascript">
    const toggleBtn = document.getElementById('toggleCode');

    function toggleCodeVisibility() {
        const codeCells = document.querySelectorAll('.jp-CodeCell');
        let isVisible = true;

        for (const codeCell of codeCells) {
            const inputWrapper = codeCell.querySelector('.jp-Cell-inputWrapper');
            if (inputWrapper && inputWrapper.style.display === 'none') {
                isVisible = false;
                break;
            }
        }

        for (const codeCell of codeCells) {
            const inputWrapper = codeCell.querySelector('.jp-Cell-inputWrapper');
            const outputPrompt = codeCell.querySelector('.jp-OutputPrompt');
            const outputAreaPrompt = codeCell.querySelector('.jp-OutputArea-prompt');
            const inputPrompt = codeCell.querySelector('.jp-InputPrompt');
            const inputAreaPrompt = codeCell.querySelector('.jp-InputArea-prompt');

            if (inputWrapper) {
                if (isVisible) {
                    inputWrapper.style.display = 'none';
                    toggleBtn.textContent = 'Afficher Code';
                    if (outputPrompt) outputPrompt.style.visibility = 'hidden';
                    if (outputAreaPrompt) outputAreaPrompt.style.visibility = 'hidden';
                    if (inputPrompt) inputPrompt.style.visibility = 'hidden';
                    if (inputAreaPrompt) inputAreaPrompt.style.visibility = 'hidden';
                } else {
                    inputWrapper.style.display = 'block';
                    toggleBtn.textContent = 'Cacher Code';
                    if (outputPrompt) outputPrompt.style.visibility = 'visible';
                    if (outputAreaPrompt) outputAreaPrompt.style.visibility = 'visible';
                    if (inputPrompt) inputPrompt.style.visibility = 'visible';
                    if (inputAreaPrompt) inputAreaPrompt.style.visibility = 'visible';
                }
            }
        }
    }

    toggleBtn.addEventListener('click', toggleCodeVisibility);

    toggleCodeVisibility();
</script>
```





