****
## UNIVERSITÉ RENNES 1  
## Master 1 : Stratégies Digitales et Innovations Numériques  

## Etude sur les usages problématique d’internet. 
****


>La base de donnée n'étant pas libre, elle ne peut pas etre fourni ici !  


# PARTIE 1
>Quel est l'impacte du flow sur les usages problématique d’internet ?  


## Résumé

Le flow est définie comme étant un état psychologie de satisfaction maximal et semble potentiellement liées à l’addiction que peuvent éprouver certains individus en ce qui concerne leurs usages d’internet. L’ensemble du dossier tend à mesurer l’impact du flow sur les usages problématique d’internet (UPI). L’échantillon est constitué de deux sous populations ; les joueurs et les non joueurs de jeux vidéo. Plusieurs modèle ont été réalisé et l’on a vérifié que le flow augmentait bien les UPI. De plus nous avons observé que la participation du flow sur les UPI n’est pas uniquement liée à la pratique des jeux vidéo mais sur l’ensemble de la population.  

## Mots clés
Flow, Usages problématiques d’internet, Jeux vidéos, Psychométrie  

## Abstract

The flow is defined as being a psychological state of maximum satisfaction and seems potentially linked to the addiction that some individuals may experience with regard to their use of the Internet. The entire file tends to measure the impact of flow on problematic Internet use (UPI). The sample consists of two sub-populations; video game players and non-players. Several models were carried out and it was verified that the flow increased the IPUs. Moreover we observed that the participation of the flow on the IPU is not only related to the practice of the video games but on the whole of the population.  

## Keywords
Flow, Problematic Uses of the Internet, Video Games, Psychometrics  

## Descriptifs des variables

|   Variables  |                                    Descriptions                                    |
|:------------ |:---------------------------------------------------------------------------------- |
| UPI          | Usages problématique d’internet                                                    |
| JV           | Variable binaire vaut 1 si l’individu est un joueur et 0 sinon                     |
| Flow         | Variables psychologique obtenus en faisant la moyenne des items                    |
| Antecedent   | Variables psychologique obtenus en faisant la moyenne des items                    |
| JVFlow       | Variable croisé entre JV et Flow                                                   |
| JVAntecedent | Variable croisé entre JV et Antecedent                                             |
| Sexe         | Correspond au sexe de l’individu vaut 1 si c’est une Femme et 0 si c’est un Homme  |
| Age          | Correspond à l’age des individus                                                   |
| Inf_bac      | Inf_Bac vaut 1 pour les niveaux de diplôme strictement inférieur au Bac et 0 sinon |
| Sup_bac      | Sup_Bac vaut 1 si les individus de diplôme strictement supérieur au Bac et 0 sinon |
| Moins_2000   | Moins_2000 vaut 1 si les individus on moins de 2000 € par mois et 0 sinon          |
| Plus_2000    | Plus_2000 vaut 1 si leurs revenus est supérieur à 2000 € par mois et 0 sinon       |    
