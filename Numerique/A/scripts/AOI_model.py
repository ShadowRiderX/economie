###################################################################################################
## UNIVERSITÉ RENNES 1 
## Master 1 : Stratégies Digitales et Innovations Numériques
##
## Quel est l'impacte du flow sur les usages problématique d’internet ?
##
## Auteur : ALEXIS IVORRA
###################################################################################################

# Chargement des librairies
import pandas
import statsmodels.formula.api as smf

# Chargement des données
df = pandas.read_excel("AOI_data.xls", sheet_name="data")
print(df.describe())

# Connaitre les données manquantes
print(df.isnull().sum())

# Dimention de la data frame
print(df.shape)

# Supréssion des données manquantes
df = df.dropna()

# Modèle A
print("Modele A")
estimation_ols_a = smf.ols(formula="UPI ~ JV + FLOW + ANT", data=df).fit()
print(estimation_ols_a.summary())
with open('modele_a.csv', 'w') as fa:
    fa.write(estimation_ols_a.summary().as_csv())
print("\n\n")

# Modèle B
print("Modele B")
estimation_ols_b = smf.ols(formula="UPI ~ JV + FLOW + ANT + JVANT + JVFLOW", data=df).fit()
print(estimation_ols_b.summary())
with open('modele_b.csv', 'w') as fb:
    fb.write(estimation_ols_b.summary().as_csv())
print("\n\n")


# Créations des dummies
df['SEXE'] = 1
df.loc[df['SEXE'] == '1', 'SEXE'] = 0

# Variable binaire niveau de diplome : Inf_Bac et Sup_Bac
df['Inf_Bac'] = 1
df.loc[(df['REVENUS'] >= 1) & (df['REVENUS'] < 5), 'Inf_Bac'] = 0

df['Sup_Bac'] = 1
df.loc[(df['REVENUS'] > 5) & (df['REVENUS'] <= 9), 'Sup_Bac'] = 0

# Variable binaire niveau de revenus : Moins_2000 et Plus_2000
df['Moins_2000'] = 1
df.loc[(df['REVENUS'] >= 1) & (df['REVENUS'] < 6), 'Moins_2000'] = 0

df['Plus_2000'] = 1
df.loc[(df['REVENUS'] > 6) & (df['REVENUS'] <= 13), 'Plus_2000'] = 0


# Model C
print("Modele C")
estimation_ols_c = smf.ols(formula="UPI ~ JV + SEXE + AGE + ANT + FLOW + Inf_Bac + Sup_Bac + Moins_2000 + Plus_2000", data=df).fit()
print(estimation_ols_c.summary())
with open('modele_c.csv', 'w') as fc:
    fc.write(estimation_ols_c.summary().as_csv())
print("\n\n")

# Modèle D
print("Modele D")
estimation_ols_d = smf.ols(formula="UPI ~ JV + SEXE + AGE + ANT + FLOW + Inf_Bac + Sup_Bac + Moins_2000 + Plus_2000 + JVFLOW + JVANT", data=df).fit()
print(estimation_ols_d.summary())
with open('modele_d.csv', 'w') as fd:
    fd.write(estimation_ols_d.summary().as_csv())
print("\n\n")











