###################################################################################################
## UNIVERSITÉ RENNES 1 
## Master 1 : Stratégies Digitales et Innovations Numériques
##
## Quel est l'impacte du flow sur les usages problématique d’internet ?
##
## Auteur : ALEXIS IVORRA
###################################################################################################

# Chargement des librairies
import pandas
import matplotlib.pyplot as plt
import seaborn as sns

# Chargement des données
df_a = pandas.read_excel("AOI_data.xls", sheet_name="data")
print(df_a.describe())

# Definition des variables
Y = df_a['UPI']
x_JV = df_a['JV']
x_ANT = df_a['ANT']
x_FLOW = df_a['FLOW']


# GRAPHIQUES DES DISTRIBUTIONS
# Distribution des UPI
A = df_a.groupby(['JV', 'UPI'])['UPI'].sum()
fig_a_upi, ax_upi = plt.subplots()
ax_upi.plot(A[0], 'b', alpha=0.7, label='Non Joueurs')
ax_upi.plot(A[1], 'r', alpha=0.7, label='Joueur')
ax_upi.set_xlabel(r"Valeurs des UPI", fontsize=10)
ax_upi.set_ylabel(r"Sommes des UPI", fontsize=10)
ax_upi.set_title("Distributions des UPI", fontsize=15)
ax_upi.grid(True)
fig_a_upi.tight_layout()
plt.legend()
plt.show()

# Distribution du FLOW
A = df_a.groupby(['JV', 'FLOW'])['FLOW'].sum()
fig_a_upi, ax_upi = plt.subplots()
ax_upi.plot(A[0], 'b', alpha=0.7, label='Non Joueurs')
ax_upi.plot(A[1], 'r', alpha=0.7, label='Joueur')
ax_upi.set_xlabel(r"Valeurs du FLOW", fontsize=10)
ax_upi.set_ylabel(r"Sommes du FLOW", fontsize=10)
ax_upi.set_title("Distributions du FLOW", fontsize=15)
ax_upi.grid(True)
fig_a_upi.tight_layout()
plt.legend()
plt.show()

# Distributions des Antécédents
A = df_a.groupby(['JV', 'ANT'])['ANT'].sum()
fig_a_upi, ax_upi = plt.subplots()
ax_upi.plot(A[0], 'b', alpha=0.7, label='Non Joueurs')
ax_upi.plot(A[1], 'r', alpha=0.7, label='Joueur')
ax_upi.set_xlabel(r"Valeurs des Antécédents", fontsize=10)
ax_upi.set_ylabel(r"Sommes des Antécédents", fontsize=10)
ax_upi.set_title("Distributions des Antécédents", fontsize=15)
ax_upi.grid(True)
fig_a_upi.tight_layout()
plt.legend()
plt.show()

# GRAPHIQUES DES ÉVOLUTIONS CROISÉS
# Évolution de la moyenne des UPI en fonction du flow
A = df_a.groupby(['JV', 'FLOW'])['UPI'].mean()
fig_a_upi, ax_upi = plt.subplots()
ax_upi.plot(A[0], 'b', alpha=0.7, label='Non Joueurs')
ax_upi.plot(A[1], 'r', alpha=0.7, label='Joueur')
ax_upi.set_xlabel(r"Valeurs du FLOW", fontsize=10)
ax_upi.set_ylabel(r"Moyennes des UPI", fontsize=10)
ax_upi.set_title("Évolutions des UPI en fonction du FLOW", fontsize=15)
ax_upi.grid(True)
fig_a_upi.tight_layout()
plt.legend()
plt.show()

# Distribution des UPI en fonction du flow
A = df_a.groupby(['JV', 'FLOW'])['UPI'].sum()
fig_a_upi, ax_upi = plt.subplots()
ax_upi.plot(A[0], 'b', alpha=0.7, label='Non Joueurs')
ax_upi.plot(A[1], 'r', alpha=0.7, label='Joueur')
ax_upi.set_xlabel(r"Valeurs du FLOW", fontsize=10)
ax_upi.set_ylabel(r"Sommes des UPI", fontsize=10)
ax_upi.set_title("Distributions des UPI en fonction du FLOW", fontsize=15)
ax_upi.grid(True)
fig_a_upi.tight_layout()
plt.legend()
plt.show()

# Évolution de la moyenne des UPI en fonction des Antécédents
A = df_a.groupby(['JV', 'ANT'])['UPI'].mean()
fig_a_upi, ax_upi = plt.subplots()
ax_upi.plot(A[0], 'b', alpha=0.7, label='Non Joueurs')
ax_upi.plot(A[1], 'r', alpha=0.7, label='Joueur')
ax_upi.set_xlabel(r"Valeurs des Antécédents", fontsize=10)
ax_upi.set_ylabel(r"Moyennes des UPI", fontsize=10)
ax_upi.set_title("Évolutions des UPI en fonction des Antécédents", fontsize=15)
ax_upi.grid(True)
fig_a_upi.tight_layout()
plt.legend()
plt.show()

# Distribution des UPI en fonction du flow
A = df_a.groupby(['JV', 'ANT'])['UPI'].sum()
fig_a_upi, ax_upi = plt.subplots()
ax_upi.plot(A[0], 'b', alpha=0.7, label='Non Joueurs')
ax_upi.plot(A[1], 'r', alpha=0.7, label='Joueur')
ax_upi.set_xlabel(r"Valeurs des Antécédents", fontsize=10)
ax_upi.set_ylabel(r"Sommes des UPI", fontsize=10)
ax_upi.set_title("Distributions des UPI en fonction des Antécédents", fontsize=15)
ax_upi.grid(True)
fig_a_upi.tight_layout()
plt.legend()
plt.show()

# GRAPHIQUES DES DENSITÉS
# Densité des UPI
A = df_a.groupby(['JV', 'UPI'])['UPI'].sum()
sns.kdeplot(A[0], color='blue', shade=True, label='Non Joueurs')
sns.kdeplot(A[1], color='red', shade=True, label='Joueur')
plt.title('Densités des UPI')
plt.legend()
plt.show()

# Densité du Flow
A = df_a.groupby(['JV', 'FLOW'])['FLOW'].sum()
sns.kdeplot(A[0], color='blue', shade=True, label='Non Joueurs')
sns.kdeplot(A[1], color='red', shade=True, label='Joueur')
plt.title('Densités des FLOW')
plt.legend()
plt.show()

# Densité des Antécédents
A = df_a.groupby(['JV', 'ANT'])['ANT'].sum()
sns.kdeplot(A[0], color='blue', shade=True, label='Non Joueurs')
sns.kdeplot(A[1], color='red', shade=True, label='Joueur')
plt.title('Densités des ANT')
plt.legend()
plt.show()

















