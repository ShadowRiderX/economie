# ÉCONOMIE


## 1. Microéconomie

### 1.1 Brownien
Etude sur les mouvements Brownien en liens avec l'économie.
Le mouvement brownien est un processus gaussien, à accroissements indépendants, stationnaires : son accroissement Wt−Ws (s strictement inférieur à t, tous les deux positifs) suit une loi gaussienne centrée, de variance (t−s)
[Plus d'information](https://images.math.cnrs.fr/Les-mathematiques-appliquees-au.html "CNRS")

### 1.2 OffreDemande
Application R-Shiny de visualisation de marché en concurrence pure et parfaite selon la théorie microéconomique.

### 1.3 CournotNash
Scrypts pour déterminer l'équilibre de Nash dans un jeu à la Cournot


### 1.4 Mumford-Shah_Hausdorff
Etudes sur la Fonctionnelle de Mumford-Shah et la Distance de Hausdorff


## 2. QuantEcon
Test de la librairie QuantEcon et de certains script.
[Plus d'information](https://quantecon.org/ "QuantEcon")



## 3. Numerique
Etudes socio-économique autour des usages du numérique.
Scripts d'analyse statistique, analyse factoriel, classifieur et modèles psychométrique.



## 4. Population
Application de visualisation d'évolution de la poplation : test de la librairie python folium.



## 5. Strategies

### 5.1. Sociogramme
Sociogrammes de Moreno

### 5.2. Byzantin
Systèmes d'accords byzantins



## 6. Crypto

### 6.1. Etudes sur les séries temporelles
* Moyenne mobile autorégressive (ARMA)
* Moyenne mobile intégrée autorégressive (ARIMA)
* ARIMA saisonnier (SARIMA)

### 6.2. Création de Graphiques
* Graphique simple (Bar, Dudonut, ...) avec Plotly
* Graphique avançé (Candlestick, RSI...)

### 6.3 ShadowCoin
* Script d'une blockchain

### 6.4 Application Flask
* Stockfolio de crypto 
* Application de visualisation du cours des crypto : HelloCrypto basé sur l'API investpy 

ATTENTION: 
historical data de investpy ne fonctionne plus depuis ... : ConnectionError: ERR#0015: error 403, try again later. #600



