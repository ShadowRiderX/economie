import numpy as np
import matplotlib.pyplot as plt
from hmmlearn import hmm

# Simulation Brownienne
def brownien_simulation(x0, n, dt, sigma):
    x0 = np.asarray(x0)
    out = np.empty(n + 1)
    out[0] = x0
    for i in range(1, n + 1):
        out[i] = out[i - 1] + np.random.normal(loc=0.0, scale=sigma * np.sqrt(dt))
    return out

# Paramètres de simulation
x0 = 32193  # Valeur initiale
n = 100     # Nombre d'étapes
dt = 1      # Pas de temps
sigma = 0.1 # Volatilité

# Génération du mouvement brownien
x = np.arange(n + 1)
y = brownien_simulation(x0, n, dt, sigma)

# Préparation des données pour HMM
# On utilise les variations entre chaque point comme observations
returns = np.diff(y).reshape(-1, 1)  # Rendements entre chaque pas de temps

# Définition d'un HMM avec 2 états (hausse et baisse)
model = hmm.GaussianHMM(n_components=2, covariance_type="diag", n_iter=100)
model.fit(returns)

# Prédiction des états cachés
hidden_states = model.predict(returns)

# Visualisation
plt.figure(figsize=(12, 6))
plt.plot(x, y, label="Mouvement Brownien", color="black", linewidth=1.5)

# Tracer les zones de tendance en couleur
for i in range(1, len(x)):
    color = "lightcoral" if hidden_states[i - 1] == 1 else "lightgreen"
    plt.axvspan(x[i - 1], x[i], color=color, alpha=0.3)

plt.xlabel("Temps")
plt.ylabel("Valeur")
plt.title("Segmentation d'un Mouvement Brownien avec un HMM")
plt.legend()
plt.show()





