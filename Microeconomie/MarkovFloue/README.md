# 1. Chaînes de Markov et Chaînes de Markov Cachées (HMM)

## 1.1 Chaînes de Markov

Une chaîne de Markov est un processus stochastique qui suit la propriété de Markov, c'est-à-dire que l'état futur d'un système dépend uniquement de son état actuel et non de la séquence des états précédents.  

Caractéristiques principales :  

* Un ensemble fini ou infini d'états S={s1,s2,...,sn}.  
* Une matrice de transition P, où chaque élément Pij représente la probabilité de passer de l'état "si​" à l'état "sj"​.  
* Une distribution initiale π qui définit la probabilité initiale d'être dans chaque état.  

[Sources wikipédia](https://en.wikipedia.org/wiki/Markov_chain)


## 1.2 Chaînes de Markov Cachées (HMM - Hidden Markov Models)

Une chaîne de Markov cachée (HMM) est une extension des chaînes de Markov où les états réels du système sont cachés (non observables). On observe seulement une séquence de sorties (émissions) qui dépendent des états cachés.  

Composants d'un HMM :  

* États cachés : S={s1,s2,...,sn}.  
* Matrice de transition A : Probabilités de passage entre états cachés.  
* Observations (sorties visibles) : O={o1,o2,...,om}.  
* Matrice d’émission B : Probabilité qu’un état caché si​ génère une observation oj​.  
* Distribution initiale π : Probabilités initiales des états cachés.  

[Sources wikipédia](https://en.wikipedia.org/wiki/Hidden_Markov_model)


## 1.3. Algorithme principaux

* Problème de décodage : Trouver la séquence la plus probable d’états cachés (algorithme de Viterbi).  
* Problème d’apprentissage : Estimer les probabilités de transition et d’émission (algorithme de Baum-Welch).  


# 2. Logique Floue et Arithmétique Floue

## 2.1 Logique Floue (Fuzzy Logic)

La logique floue est une extension de la logique booléenne qui permet de gérer l’incertitude et l’imprécision en attribuant à une variable une valeur de vérité entre 0 et 1, plutôt que simplement vrai (1) ou faux (0).  

Concepts clés :  

* Ensembles flous : Un élément appartient à un ensemble avec un degré d’appartenance entre 0 et 1.  
* Fonctions d’appartenance : Définissent à quel degré un élément appartient à un ensemble flou.  
* Opérateurs flous : Généralisations des opérateurs logiques classiques (ET, OU, NON).  
* Règles floues (IF-THEN)  
* Défuzzification : Transformation des valeurs floues en valeurs précises.  


## 2.2 Arithmétique Floue (Fuzzy Arithmetic)

L’arithmétique floue est une généralisation de l’arithmétique classique qui permet de manipuler des nombres flous, c'est-à-dire des valeurs ayant une incertitude.  

Opérations avec des nombres flous :  

* Les nombres flous sont souvent représentés par des triplets ou des fonctions d’appartenance.  
* Exemple d’un nombre flou triangulaire : A~=(a,b,c) avec :  
    a est la borne inférieure,  
    b est la valeur centrale (plus probable),  
    c est la borne supérieure.  


## Librairies Utilisés

* [scikit-fuzzy](https://github.com/scikit-fuzzy/scikit-fuzzy)  
* [hmmlearn](https://hmmlearn.readthedocs.io/en/latest/)  





