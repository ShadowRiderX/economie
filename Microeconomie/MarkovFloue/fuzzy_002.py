import numpy as np
import matplotlib.pyplot as plt
import skfuzzy as fuzz
import skfuzzy.membership as mf

# Simulation Brownienne
def brownien_simulation(x0, n, dt, sigma):
    x0 = np.asarray(x0)
    out = np.empty(n + 1)
    out[0] = x0
    for i in range(1, n + 1):
        out[i] = out[i - 1] + np.random.normal(loc=0.0, scale=sigma * np.sqrt(dt))
    return out

# Paramètres de simulation
x0 = 32193  # Valeur initiale
n = 100     # Nombre d'étapes
dt = 1      # Pas de temps
sigma = 0.1 # Volatilité

# Génération du mouvement brownien
x = np.arange(n + 1)
y = brownien_simulation(x0, n, dt, sigma)

# Calcul des rendements
returns = np.diff(y)  # Variations entre chaque point

# Définition des ensembles flous (Hausse/Baisse)
x_range = np.linspace(min(returns), max(returns), 100)

baisse = mf.trimf(x_range, [min(returns), min(returns), 0])  # Fonction d'appartenance à la baisse
hausse = mf.trimf(x_range, [0, max(returns), max(returns)])  # Fonction d'appartenance à la hausse

# Classification floue en hausse ou baisse
tendances = []
for r in returns:
    deg_baisse = fuzz.interp_membership(x_range, baisse, r)
    deg_hausse = fuzz.interp_membership(x_range, hausse, r)

    # Sélection de la tendance dominante
    tendances.append(0 if deg_baisse > deg_hausse else 1)  # 0 = baisse, 1 = hausse

# Affichage des tendances
plt.figure(figsize=(12, 6))
plt.plot(x, y, label="Mouvement Brownien", color="black", linewidth=1.5)

# Couleurs selon la tendance (baisse = rouge, hausse = vert)
colors = ["lightcoral", "lightgreen"]
for i in range(1, len(x)):
    plt.axvspan(x[i - 1], x[i], color=colors[tendances[i - 1]], alpha=0.3)

plt.xlabel("Temps")
plt.ylabel("Valeur")
plt.title("Segmentation d'un Mouvement Brownien avec Logique Floue (Hausse/Baisse)")
plt.legend()
plt.show()





