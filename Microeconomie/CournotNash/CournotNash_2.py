import sympy as sy
import dash
from dash import dcc, html
from dash.dependencies import Input, Output
import plotly.graph_objs as go


app = dash.Dash(__name__)

app.layout = html.Div([
    html.H1("Courbes de réaction de F1 et F2"),
    dcc.Input(id='param-f1', type='text', placeholder='Entrez F1', value='12*q1 - q1**2 - q1*q2'),
    dcc.Input(id='param-f2', type='text', placeholder='Entrez F2', value='12*q2 - q2**2 - q2*q1'),
    dcc.Graph(id='function-plot')
])

@app.callback(
    Output('function-plot', 'figure'),
    [Input('param-f1', 'value'), Input('param-f2', 'value')]
)

def update_function_plot(param_f1, param_f2):
    if param_f1 and param_f2:
        # Définitions des paramètres
        N = 2
        q1 = sy.Symbol('q1')
        q2 = sy.Symbol('q2')

        f1_expr = sy.sympify(param_f1)
        f2_expr = sy.sympify(param_f2)

        def F1(q1, q2):
            f1 = f1_expr
            return f1

        def F2(q1, q2):
            f2 = f2_expr
            return f2

        # Calcule des dérivées
        derivation_1 = sy.diff(F1(q1, q2), q1)
        derivation_2 = sy.diff(F2(q1, q2), q2)
        print(f"La dérivé de F1 est : {derivation_1}")
        print(f"La dérivé de F2 est : {derivation_2}")

        # Calcule des extremum local
        extrema_1 = sy.solve(derivation_1, q1)
        extrema_2 = sy.solve(derivation_2, q2)
        print(f"L'extremum local de F1 est : {extrema_1[0]}")
        print(f"L'extremum local de F2 est : {extrema_2[0]}")

        # Calcule de l'equilibre de Nash
        eq1 = sy.Eq(extrema_1[0], q1)
        eq2 = sy.Eq(extrema_2[0], q1)
        solution = sy.solve((eq1, eq2), (q2, q1))
        print(f"L'equilibre de Nash est : {solution}")

        # Calcule des quantitées monopolistique
        f1m = {'q1': extrema_1[0].subs(q2, 0), 'q2': 0}
        f2m = {'q1': 0, 'q2': extrema_2[0].subs(q1, 0)}
        print(f"Si F1 est un monopole alors : {f1m}")
        print(f"Si F2 est un monopole alors : {f2m}")

        # Inversement de l'égalité pour F2
        inverted_eq = sy.Eq(q2, extrema_2[0])
        inverted_solution = sy.solve(inverted_eq, q1)
        print(inverted_solution[0])

        # Génèration des valeurs de x pour les courbes
        max = (int(f1m['q1'])*N)+2
        x_values = list(range(0, max))

        # Définitions des fonctions réactions à traçer
        extrema_function_1 = sy.lambdify(q2, extrema_1[0], 'numpy')
        extrema_function_2 = sy.lambdify(q2, inverted_solution[0], 'numpy')

        # Calcules des valeurs de y des fonctions de réaction
        y1_values = [extrema_function_1(x) for x in x_values]
        y2_values = [extrema_function_2(x) for x in x_values]

        # Affichage des Fonctions de réactions et de l'équilibre de Nash
        trace1 = go.Scatter(x=x_values, y=y2_values, mode='lines', name=f'R1(q2)', marker=dict(size=1, color='red'))
        trace2 = go.Scatter(x=x_values, y=y1_values, mode='lines', name=f'R2(q1)', marker=dict(size=1, color='blue'))
        trace3 = go.Scatter(x=[float(solution[q1])], y=[float(solution[q2])],
                            mode='markers', name=f'eq Nash', marker=dict(size=10, color='green'))

        # Affichages des stratégies possibles
        possible_q1_x = [float(f2m['q2'] / N), float(f2m['q2'])]
        possible_q1_y = [0, 0]
        strategy_possible_f1 = go.Scatter(
            x=possible_q1_x,
            y=possible_q1_y,
            mode='lines',
            name='Stratégies possible de F1',
            line=dict(color='red', width=10)
        )

        possible_q2_x = [0, 0]
        possible_q2_y = [float(f1m['q1'] / N), float(f1m['q1'])]
        strategy_possible_f2 = go.Scatter(
            x=possible_q2_x,
            y=possible_q2_y,
            mode='lines',
            name='Stratégies possible de F2',
            line=dict(color='blue', width=10)
        )

        # Affichages des stratégies dominées
        dominated_q2_x = [0, float(f1m['q1']*N)]
        dominated_q2_y = [0, 0]
        strategy_dominated_f1 = go.Scatter(
            x=dominated_q2_x,
            y=dominated_q2_y,
            mode='lines',
            name='Stratégies Dominées de F1',
            opacity=0.2,
            line=dict(color='red', width=10)
        )

        dominated_q1_x = [0, 0]
        dominated_q1_y = [0, float(f2m['q2']*N)]
        strategy_dominated_f2 = go.Scatter(
            x=dominated_q1_x,
            y=dominated_q1_y,
            mode='lines',
            name='Stratégies Dominées de F2',
            opacity=0.2,
            line=dict(color='blue', width=10)
        )

        # f1 = 12*q1 - q1**2 - q1*q2
        # f2 = 15*q2 - q2**2 - q2*q1

        # Paramètre du layout
        layout = go.Layout(
            title=f'Courbes de réaction de F1 et F2',
            xaxis=dict(title='Q1', range=[0, max], scaleratio=1),
            yaxis=dict(title='Q2', range=[0, max], scaleratio=1),
            margin=dict(pad=1)
        )

        return {'data': [trace1, trace2, trace3,
                         strategy_possible_f1, strategy_possible_f2,
                         strategy_dominated_f1, strategy_dominated_f2], 'layout': layout}
    else:
        return {}

if __name__ == '__main__':
    app.run_server(debug=True)
