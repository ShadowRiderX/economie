# Concurrence duopolistique à la Cournot

# Chargement des librairies
import matplotlib.pyplot as plt
import numpy as np
import sympy as sy

# Jeu d'entreprises
## Nombre de Firme
N = 2

## Fonction de prix des firmes F1 et F2
def F1(q1, q2):
    f1 = 12*q1 - q1**2 - q1*q2
    return f1

def F2(q1, q2):
    f2 = 12*q2 - q2**2 - q2*q1
    return f2

## Définition des variables mathématiques pour chqu'une des stratéges des firmes
q1 = sy.Symbol('q1')
q2 = sy.Symbol('q2')

## Etape 1 : Calculer les dérivés des fonctions
"""
Fonction de meilleurs réponse à une stratégie arbitraire de son rivale
"""

derivation_1 = sy.diff(F1(q1, q2), q1)
derivation_2 = sy.diff(F2(q1, q2), q2)
print(f"La dérivé de F1 est : {derivation_1}")
print(f"La dérivé de F2 est : {derivation_2}")

## Etape 2 : Détermination des extremums local en annulant les dérivés 1er
extrema_1 = sy.solve(derivation_1, q1)
extrema_2 = sy.solve(derivation_2, q2)
print(f"L'extremum local de F1 est : {extrema_1[0]}")
print(f"L'extremum local de F2 est : {extrema_2[0]}")

## Etape 3 : Détermination de S* l'équilibre tel que S1*=S2*
eq1 = sy.Eq(extrema_1[0], q1)
eq2 = sy.Eq(extrema_2[0], q1)
solution = sy.solve((eq1, eq2), (q2, q1))
print(f"L'equilibre de Nash est : {solution}")


# Situation monopolistique
## Retrouver les valeurs f1m, f2m et nash
f1m = {'q1': extrema_1[0].subs(q2, 0), 'q2': 0}
f2m = {'q1': 0, 'q2': extrema_2[0].subs(q1, 0)}
nash = {'q1': solution[q1], 'q2': solution[q2]}

print(f"Si F1 est un monopole alors : {f1m}")
print(f"Si F2 est un monopole alors : {f2m}")

## Fonctions de réaction de F1 et F2
def reaction_F1(q2):
    return sy.solve(F1(q1, q2).diff(q1), q1)

def reaction_F2(q1):
    return sy.solve(F2(q1, q2).diff(q2), q2)

## Calcul des extrémités des courbes de réaction de F1 et F2
q1_min = min(0, extrema_1[0].evalf(subs=f1m) if extrema_1 else 0)
q1_max = max(0, extrema_1[0].evalf(subs=f1m) if extrema_1 else 0)
q2_min = min(0, extrema_2[0].evalf(subs=f2m) if extrema_2 else 0)
q2_max = max(0, extrema_2[0].evalf(subs=f2m) if extrema_2 else 0)

## Convertir en nombres entiers
q1_min = int(q1_min)
q1_max = int(q1_max)
q2_min = int(q2_min)
q2_max = int(q2_max)

## Utiliser ces valeurs pour générer q2_values
q2_values = np.linspace(0, q2_max*N+2, 100)

## Calcul des courbes de réaction de F1 et F2
r1_values = [reaction_F1(q2) for q2 in q2_values]
r2_values = [reaction_F2(q1) for q1 in q2_values]

## Évaluation des courbes de réaction avec les points de coordination
r1_values_evaluated = [r[0].evalf(subs=f1m) for r in r1_values]
r2_values_evaluated = [r[0].evalf(subs=f2m) for r in r2_values]


# GRAPHIQUE

plt.figure(figsize=(10, 10))

## Affichages des stratégies possibles
possible_q2_x = [0, 0.2, 0.2, 0]
possible_q2_y = [f1m['q1']/N, f1m['q1']/N, f1m['q1'], f1m['q1']]
plt.fill(possible_q2_x, possible_q2_y, color='blue', alpha=1, label='Stratégies Possibles de F1')

possible_q1_x = [f2m['q2']/N, f2m['q2']/N, f2m['q2'], f2m['q2']]
possible_q1_y = [0, 0.2, 0.2, 0]
plt.fill(possible_q1_x, possible_q1_y, color='red', alpha=1, label='Stratégies Possibles de F2')


## Affichages des stratégies dominées
dominated_q2_x1 = [0, 0.2, 0.2, 0]
dominated_q2_y = [0, 0, f2m['q2']*N, f2m['q2']*N]
plt.fill(dominated_q2_x1, dominated_q2_y, color='blue', alpha=0.2, label='Stratégies Dominées de F1')

dominated_q1_x1 = [0, 0, f1m['q1']*N, f1m['q1']*N]
dominated_q1_y = [0, 0.2, 0.2, 0]
plt.fill(dominated_q1_x1, dominated_q1_y, color='red', alpha=0.2, label='Stratégies Dominées de F2')


## Affichages des courbes de réactions et équilibre
plt.xlim(0, q1_max*N + 2)
plt.ylim(0, q2_max*N + 2)
plt.plot(q2_values, r1_values_evaluated, label='Réaction F1', color='blue')
plt.plot(r2_values_evaluated, q2_values, label='Réaction F2', color='red')
plt.scatter(f1m['q2'], f1m['q1'], color='blue', label='Quantitées Monopolistique de F1', s=100)
plt.scatter(f2m['q2'], f2m['q1'], color='red', label='Quantitées Monopolistique de F2', s=100)
plt.scatter(nash['q1'], nash['q2'], color='green', label='Équilibre de Nash', s=100)
plt.xlabel('Q1')
plt.ylabel('Q2')
plt.legend()
plt.gca().set_aspect('equal')
plt.grid(True, linestyle='--', alpha=0.5)
plt.title('Courbes de Réaction et Équilibre de Nash')
plt.show()



