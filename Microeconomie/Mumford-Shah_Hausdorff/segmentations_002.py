import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import find_peaks
from scipy.spatial.distance import directed_hausdorff

# Simulation Brownienne
def brownien_simulation(x0, n, dt, sigma):
    x0 = np.asarray(x0)
    out = np.empty(n + 1)
    out[0] = x0
    for i in range(1, n + 1):
        out[i] = out[i - 1] + np.random.normal(loc=0.0, scale=sigma * np.sqrt(dt))
    return out

# Fonctionnelle de Mumford-Shah (approximation par différenciation)
def segment_mumford_shah(y, lambda_reg=50):
    dy = np.gradient(y)
    threshold = np.mean(np.abs(dy)) + lambda_reg * np.std(dy)  # Seuil adaptatif
    peaks, _ = find_peaks(np.abs(dy), height=threshold)
    return np.concatenate(([0], peaks, [len(y)-1]))  # Ajout des extrémités

# Distance de Hausdorff pour fusionner les segments proches
def hausdorff_fusion(segments, y, threshold=5):
    fused_segments = [segments[0]]
    for i in range(1, len(segments)):
        dH = directed_hausdorff(y[segments[i-1]:segments[i]].reshape(-1, 1),
                                y[segments[i]:segments[min(i+1, len(segments)-1)]].reshape(-1, 1))[0]
        if dH > threshold:  # Seuil pour séparer les segments significatifs
            fused_segments.append(segments[i])
    return fused_segments if len(fused_segments) > 2 else segments  # Évite d'écraser segments inutilement

# Étape 1 : Simulation
x0 = 32193  # Valeur initiale
n = 100     # Nombre de jours
dt = 1      # Pas de temps
sigma = 0.1 # Volatilité
y = brownien_simulation(x0, n, dt, sigma)
x = np.arange(0, n+1)

# Étape 2 : Segmentation avec Mumford-Shah
segments = segment_mumford_shah(y, lambda_reg=0.5)
segments = hausdorff_fusion(segments, y, threshold=2)

# Étape 3 : Affichage du graphique
plt.figure(figsize=(12, 8))
plt.plot(x, y, color='white', label="Simulation 0-100 jours", linewidth=1.5)

# Tracer les zones colorées correctement
for i in range(len(segments) - 1):
    start, end = segments[i], segments[i + 1]
    avg_trend = np.mean(np.gradient(y[start:end]))  # Moyenne des variations sur le segment
    color = "lightgreen" if avg_trend > 0 else "lightcoral"
    plt.axvspan(x[start], x[end], color=color, alpha=0.3)

# Configuration du style
plt.title("Segmentation Brownienne avec Mumford-Shah et Hausdorff", color='green')
plt.xlabel("Jours")
plt.ylabel("Valeur")
plt.grid(True, color='#555555')
plt.gcf().set_facecolor('black')
plt.gca().set_facecolor('black')
plt.gca().tick_params(axis='x', colors='white')
plt.gca().tick_params(axis='y', colors='white')
plt.legend()

plt.tight_layout()
plt.show()





