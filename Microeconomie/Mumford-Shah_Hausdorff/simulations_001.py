import numpy as np
import matplotlib.pyplot as plt

# Simulation Brownienne
def brownien_simulation(x0, n, dt, sigma):
    x0 = np.asarray(x0)
    out = np.empty(n + 1)
    out[0] = x0
    for i in range(1, n + 1):
        out[i] = out[i - 1] + np.random.normal(loc=0.0, scale=sigma * np.sqrt(dt))
    return out

# Étape 1 : Simulation de base (jours 0 à 100)
x0 = 32193
n = 100
dt = 1
sigma = 0.1

simulations_0_100 = brownien_simulation(x0, n, dt, sigma)

# Étape 2 : Simulation de scénarios alternatifs (jours 101 à 200)
scenarios = [
    {"x0": simulations_0_100[-1], "n": 100, "dt": 1, "sigma": 0.2},
    {"x0": simulations_0_100[-1], "n": 100, "dt": 1, "sigma": 0.05},
    {"x0": simulations_0_100[-1], "n": 100, "dt": 1, "sigma": 0.15},
    {"x0": simulations_0_100[-1], "n": 100, "dt": 1, "sigma": 0.1},
]

simulations_scenarios = [brownien_simulation(**scenario) for scenario in scenarios]

# Étape 3 : Visualisation
plt.figure(figsize=(12, 8))

# Graphique combiné pour les jours 0 à 200
combined_days = np.arange(0, 201)

# Simulation de base
plt.plot(np.arange(0, 101), simulations_0_100, color='lightgrey', label="Simulation 0-100 jours")

# Scénarios alternatifs
colors = ['red', 'blue', 'yellow', 'green']
for i, scenario in enumerate(simulations_scenarios):
    plt.plot(np.arange(101, 201), scenario[1:], label=f"Scénario {i + 1}", color=colors[i])

plt.title("Simulations Brownienne (0 à 200 jours, incluant scénarios alternatifs)", color='Green')
plt.xlabel("Jours")
plt.ylabel("Valeur")
plt.grid(True, color='#555555')
plt.gcf().set_facecolor('black')
plt.gca().set_facecolor('black')
plt.gca().tick_params(axis='x', colors='white')
plt.gca().tick_params(axis='y', colors='white')
plt.legend()

plt.tight_layout()
plt.show()

