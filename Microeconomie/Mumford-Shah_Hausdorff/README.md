
# Introduction

## Sources

- INITIATION À LA FONCTIONNELLE DE MUMFORD-SHAH par Antoine Lemenant
- https://www.cmls.polytechnique.fr/xups/xups17-03.pdf
- https://www.youtube.com/watch?v=TkOj09UaoKY&themeRefresh=1


## 1. Fonctionnelle de Mumford-Shah

La fonctionnelle de Mumford-Shah est une fonctionnelle variationnelle utilisée en traitement d'image et en analyse des formes pour la détection des contours et la segmentation. 

Elle est définie comme suit :
$$
E(u, K) = \int_\Omega (u - g)^2 \, dx + \lambda \int_{\Omega \setminus K} |\nabla u|^2 \, dx + \nu |K|
$$

où :

* Ω⊂Rn est un domaine ouvert,
* g:Ω→R est une image donnée,
* u:Ω→R est l'approximation segmentée de g,
* K est un ensemble de discontinuités (courbes ou surfaces selon la dimension de l'espace),
* ∣∇u∣ est le gradient de u,
* λ,ν>0 sont des paramètres de pondération,
* ν∣K∣ est la mesure de Hausdorff (longueur, aire, etc.) de l'ensemble K.


## 2. Distance de Hausdorff

La distance de Hausdorff est une mesure entre deux ensembles A,B dans un espace métrique (X,d). Elle est définie comme :
* dH(A,B)=max⁡(sup⁡a∈Ad(a,B),sup⁡b∈Bd(b,A))

où :

* d(a,B)=inf⁡b∈Bd(a,b) est la distance du point a à l'ensemble B,
* d(b,A)=inf⁡a∈Ad(b,a) est la distance du point b à l'ensemble A.

En d'autres termes, dH(A,B) mesure la plus grande des distances minimales entre les points de A et B.

Propriétés :

* dH(A,B)=0 si et seulement si A=B dans le cas où A,B sont compacts.
* Il est symétrique : dH(A,B)=dH(B,A).
* Il satisfait l'inégalité triangulaire.


## 3. Lien entre les deux concepts

* Dans la fonctionnelle de Mumford-Shah, l'ensemble K représente les contours détectés. 
* La distance de Hausdorff peut être utilisée pour comparer ces contours K obtenus avec une observation ou pour mesurer l'évolution des contours d'un objet.


# Analyse mathématique d'un Mouvement Brownien avec la Fonctionnelle de Mumford-Shah et la Distance de Hausdorff

Nous avons un mouvement brownien Bt défini sur [0,100] avec une volatilité σ=0.1. L'objectif est de segmenter la courbe en identifiant des zones de hausse et des zones de baisse en utilisant :

* La fonctionnelle de Mumford-Shah pour segmenter les tendances
* La distance de Hausdorff pour comparer et valider les segments

## 1. Modélisation du Problème

Le mouvement brownien suit l’équation différentielle stochastique (EDS) :
* dBt=σdWt, B0=0

où Wt​ est un mouvement brownien standard.

Nous avons une courbe discrétisée Bt avec t={0,100} et nous voulons détecter des changements de tendance en minimisant la fonctionnelle de Mumford-Shah.


## 2. Formulation Variationnelle avec Mumford-Shah

Nous voulons approximer Bt​ par une fonction segmentée u(t), où chaque segment représente une tendance montante ou descendante. 

Nous posons :
$$
E(u, K) = \int_0^{100} (u(t) - B t)^2 \, dt + \lambda \int_{0}^{100 \setminus K} |u'(t)|^2 \, dt + \nu |K|
$$

où :

* u(t) est une approximation de Bt​, qui est lisse sauf aux discontinuités K.
* K est l’ensemble des points où la tendance change (ex: passage d’une hausse à une baisse).
* λ∫∣u′(t)∣^2dt favorise la régularité des segments.
* ν∣K∣ pénalise les segments trop nombreux.

L'optimisation de cette fonctionnelle permet de segmenter Bt​ en intervalles de tendance.


## 3. Algorithme de Segmentation

### Étape 1 : Approximation par Régularisation

On approxime Bt​ par une fonction en morceaux :

* On initialise u(t)≈Bt.
* On minimise E(u,K) en alternant :
    - Lissage uu par une équation d’Euler-Lagrange de la fonctionnelle.
    - Identification des ruptures K en détectant les zones où ∣u′(t)∣ est élevé.
    - Réajustement de u en forçant la constance sur chaque segment.

### Étape 2 : Segmentation basée sur la Distance de Hausdorff

Une fois les segments obtenus, on utilise la distance de Hausdorff pour comparer les groupes de points entre eux. Cela permet de :

* Valider les segments en comparant les zones segmentées avec celles obtenues par d'autres méthodes.
* Fusionner des segments proches si leur distance de Hausdorff est faible.

Pour deux segments S1​ et S2​, on calcule :
* dH(S1,S2)=max⁡(sup⁡x∈S1d(x,S2),sup⁡y∈S2d(y,S1))

Si dH(S1,S2)<ϵ, on fusionne S1​ et S2​.













