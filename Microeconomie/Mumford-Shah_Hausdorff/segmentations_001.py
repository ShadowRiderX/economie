import numpy as np
import matplotlib.pyplot as plt

# Simulation Brownienne
def brownien_simulation(x0, n, dt, sigma):
    x0 = np.asarray(x0)
    out = np.empty(n + 1)
    out[0] = x0
    for i in range(1, n + 1):
        out[i] = out[i - 1] + np.random.normal(loc=0.0, scale=sigma * np.sqrt(dt))
    return out

# Étape 1 : Simulation de base (jours 0 à 100)
x0 = 32193  # Valeur initiale
n = 100     # Nombre de jours
dt = 1      # Pas de temps
sigma = 0.1 # Volatilité

# Générer les données de simulation (toutes les valeurs seront dans simulations_0_100)
simulations_0_100 = brownien_simulation(x0, n, dt, sigma)

# Étape 2 : Création d'un graphique avec lignes verticales de tendance

# Utilisation des coordonnées générées par la simulation
x = np.arange(0, 101)   # Jours (axe des x)
y = simulations_0_100   # Valeurs simulées (axe des y)

plt.figure(figsize=(12, 8))

# Tracer la courbe principale
plt.plot(x, y, color='white', label="Simulation 0-100 jours", linewidth=1.5)

# Tracer les lignes verticales colorées en fonction de la tendance
for i in range(1, len(x)):
    color = "lightgreen" if y[i] > y[i - 1] else "lightcoral"
    plt.axvspan(x[i - 1], x[i], color=color, alpha=0.2)

# Configuration des labels et du style
plt.title("Simulation Brownienne avec lignes de tendance (0-100 jours)", color='green')
plt.xlabel("Jours")
plt.ylabel("Valeur")
plt.grid(True, color='#555555')
plt.gcf().set_facecolor('black')
plt.gca().set_facecolor('black')
plt.gca().tick_params(axis='x', colors='white')
plt.gca().tick_params(axis='y', colors='white')
plt.legend()

plt.tight_layout()
plt.show()




