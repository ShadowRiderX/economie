# Simulation de mouvement Brownien


import numpy as np
import matplotlib.pyplot as plt




## Simulation n°1

def brownien_simulation_A(x0, n, dt, sigma):
  x0 = np.asarray(x0)
  out = np.empty(n + 1)
  out[0] = x0
  for i in range(1, n + 1):
      out[i] = out[i - 1] + np.random.normal(loc=0.0, scale=sigma * np.sqrt(dt))
  return out

x0 = 32193
n = 25
dt = 1
sigma = 0.1

simulations = brownien_simulation_A(x0, n, dt, sigma)
print(simulations)

plt.figure(figsize=(12, 8))
ax1 = plt.subplot(211)
ax1.plot(simulations, color='lightgrey')
ax1.set_title("simulations Brownien", color='Green')

ax1.grid(True, color='#555555')
ax1.set_axisbelow(True)
ax1.set_facecolor('black')
ax1.figure.set_facecolor('#121212')
ax1.tick_params(axis='x', colors='white')
ax1.tick_params(axis='y', colors='white')

plt.show()




## Simulation n°2

def brownian_simulation_B(S0, r, sigma, T, M, I):
  dt = float(T) / M
  GBM = np.zeros((M + 1, I), np.float64)
  GBM[0] = S0
  for y in range(0,i-1):
    GBM[y,0]=S0
    for x in range(0,N-1):
        GBM[y,x+1] = GBM[y,x]*(1 + r*deltat + sigma*np.random.normal(0,deltat))
  return GBM

S0 = 100.
K = 100.
r = 0.05
sigma = 0.50
T = 1
N = 252
deltat = T / N
i = 50
discount_factor = np.exp(-r * T)

GBM = brownian_simulation_B(S0, r, sigma, T, N, i)

plt.figure(figsize=(12, 8))
ax1 = plt.subplot(211)

ax1.plot(GBM, color='Green', label='Predictions Brownien')

ax1.set_title("Simulation GBM", color='white')
ax1.grid(True, color='#555555')
ax1.set_axisbelow(True)
ax1.set_facecolor('black')
ax1.figure.set_facecolor('#121212')
ax1.tick_params(axis='x', colors='white')
ax1.tick_params(axis='y', colors='white')

plt.show()




## Simulation n°3 : Yves Hilpisch (Python for Finance chapiter 11)

def brownien_simulation_C(S0, r, sigma, T, M, I):
  dt = float(T) / M
  paths = np.zeros((M + 1, I), np.float64)
  paths[0] = S0
  for t in range(1, M + 1):
      Rand = np.random.standard_normal(I)
      paths[t] = paths[t - 1] * np.exp((r - 0.5 * sigma ** 2) * dt + sigma * np.sqrt(dt) * Rand)
  return paths

S0 = 100.
K = 100.
r = 0.05
sigma = 0.50
T = 1
N = 252
deltat = T / N
i = 50
discount_factor = np.exp(-r * T)

np.random.seed(123)
paths = brownien_simulation_C(S0, r, sigma, T, N, i)

np.average(paths[-1])

CallPayoffAverage = np.average(np.maximum(0, paths[-1] - K))
CallPayoff = discount_factor * CallPayoffAverage
print(CallPayoff)

plt.figure(figsize=(12, 8))
ax1 = plt.subplot(211)

ax1.plot(paths, color='Green', label='Predictions Brownien')

ax1.set_title("Close BTC", color='white')
ax1.grid(True, color='#555555')
ax1.set_axisbelow(True)
ax1.set_facecolor('black')
ax1.figure.set_facecolor('#121212')
ax1.tick_params(axis='x', colors='white')
ax1.tick_params(axis='y', colors='white')

plt.show()



