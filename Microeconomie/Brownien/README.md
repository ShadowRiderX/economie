# Simulation de mouvement Brownien


Le script BrownienSimulations effectue des 3 simulations de mouvement brownien.  
Ici, les mouvements brownien sont des modèles utilisé pour décrire le comportement d'un actif financier sous l'effet de l'incertitude et de la volatilité du marché.  




## Simulation n°1
La fonction brownien_simulation_A prend en entrée les paramètres suivants:  

    x0: la valeur initiale de l'actif financier  
    n: le nombre de pas de temps de la simulation  
    dt: le pas de temps de la simulation  
    sigma: la volatilité de l'actif financier  
  
La fonction retourne un tableau de n+1 éléments contenant les valeurs de l'actif financier à chaque pas de temps.  




## Simulation n°2

La fonction brownien_simulation_B prend en entrée les paramètres suivants:  

    S0: le prix de l'actif financier au temps t=0  
    r: le taux d'intérêt sans risque  
    sigma: la volatilité de l'actif financier  
    T: la durée de la simulation, en années  
    M: le nombre de pas de temps de la simulation  
    I: le nombre de simulations à effectuer  

La fonction retourne un tableau de M+1 lignes et I colonnes contenant les prix de l'actif financier à chaque pas de temps pour chaque simulation.  

Le script définit également des variables telles que N, deltat, i et discount_factor.  
    N représente le nombre de jours dans l'année  
    deltat représente le pas de temps en jours  
    i représente le nombre de simulations à effectuer  
    discount_factor représente le facteur de décountage utilisé pour mettre tous les flux de trésorerie sur une base comparable.  
    
    


## Simulation n°3 : Yves Hilpisch (Python for Finance chapiter 11)

La fonction brownien_simulation_C prend en entrée les paramètres suivants:  

    S0: le prix de l'actif financier au temps t=0  
    r: le taux d'intérêt sans risque  
    sigma: la volatilité de l'actif financier  
    T: la durée de la simulation, en années  
    M: le nombre de pas de temps de la simulation  
    I: le nombre de simulations à effectuer  

La fonction retourne un tableau de M+1 lignes et I colonnes contenant les prix de l'actif financier à chaque pas de temps pour chaque simulation.  

Le script définit également des variables telles que N, deltat, i et discount_factor.  
 
    N représente le nombre de jours dans l'année  
    deltat représente le pas de temps en jours  
    i représente le nombre de simulations à effectuer  
    discount_factor représente le facteur de décountage utilisé pour mettre tous les flux de trésorerie sur une base comparable.  


Le script utilise la fonction np.random.seed pour initialiser le générateur de nombres aléatoires avec la valeur 123. Cela garantit que les simulations génèrent toujours les mêmes valeurs aléatoires pour chaque exécution du script.  

Ensuite, le script appelle la fonction brownien_simulation_C avec les paramètres S0, r, sigma, T, N et i et stocke le résultat dans la variable paths.  

Le script calcule également la moyenne des dernières valeurs du tableau paths (c'est-à-dire les valeurs de l'actif financier au dernier pas de temps de chaque simulation) et stocke le résultat dans la variable CallPayoffAverage.  

Ensuite, le script calcule le payoff (profit ou perte) d'une option d'achat (call option) en utilisant la variable CallPayoffAverage et le facteur de décountage discount_factor. Le payoff d'une option d'achat est la différence entre le prix d'exercice de l'option et le prix de l'actif financier au moment de l'exercice, majorée du maximum de 0. La variable CallPayoff contient le résultat du calcul du payoff.  



