import matplotlib.pyplot as plt
import networkx as nx
import pickle

with open('Indiv', 'rb') as output:
    Base = pickle.load(output)

G = nx.MultiDiGraph()

for indiv in Base.keys():
    G.add_node(indiv)

# Mapping des questions avec des couleurs
question_colors = {'Q1': 'blue', 'Q2': 'green', 'Q3': 'red', 'Q4': 'black'}

# Ajoute les chemins au graphe
for indiv1 in Base.keys():
    for indiv2 in Base.keys():
        if indiv1 != indiv2:
            for question, relations in Base[indiv2]['Questions'].items():
                if indiv1 in relations:
                    color = question_colors[question]
                    G.add_edge(indiv1, indiv2, color=color, question=question)

# Défini les positions des noeuds du graphe
pos_ = nx.spring_layout(G)

# Dessine les noeuds
nx.draw_networkx_nodes(G, pos_, node_size=200, node_color='cornflowerblue')

# Dessine les chemins dirigé avec des fleches et légèrement courbé
edge_labels = {}
for edge in G.edges(data=True):
    char_1, char_2, data = edge
    x0, y0 = pos_[char_1]
    x1, y1 = pos_[char_2]

    offset = 0.1 if data['question'] in ['Q1', 'Q3'] else 0.2

    nx.draw_networkx_edges(G, pos_, edgelist=[(char_1, char_2)],
                           edge_color=data['color'],
                           width=2,
                           arrowstyle='<-',
                           arrowsize=10,
                           connectionstyle=f'arc3,rad={offset}')

# Ajoute les labels aux noeuds
node_labels = {node: Base[node]['Indiv']['name'] for node in G.nodes}
nx.draw_networkx_labels(G, pos_, labels=node_labels, font_weight='bold')

# Ajoute les labels aux chemins
nx.draw_networkx_edge_labels(G, pos_, edge_labels=edge_labels, font_color='red', bbox=dict(facecolor='white', edgecolor='white', boxstyle='round,pad=0.3'))

# Création de la légende
legend_labels = {'À choisi': 'blue', 'Pense être choisie par': 'green', 'À rejeté': 'red', 'Pense être rejeté par': 'black'}
legend_handles = [plt.Line2D([0], [0], marker='o', color='w', markerfacecolor=color, markersize=10, label=label) for label, color in legend_labels.items()]
plt.legend(handles=legend_handles, title='Légende', loc='upper left')

# Affiche le Graphique
plt.title('Graphe de Moreno')
plt.axis('off')
plt.show()
