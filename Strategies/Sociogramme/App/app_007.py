import dash
import dash_cytoscape as cyto
from dash import html, dcc
from dash.dependencies import Input, Output
import networkx as nx
import random
from itertools import combinations, permutations
import csv



app = dash.Dash(__name__)

# Créer le MultiDiGraph
G = nx.MultiDiGraph()

# Ajouter les nœuds
nodes = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I']
G.add_nodes_from(nodes)

# Définir les attributs pour chaque nœud
nodes_attributes = {
    'A': {'Nom': 'A', 'Prenom': 'a'},
    'B': {'Nom': 'B', 'Prenom': 'b'},
    'C': {'Nom': 'C', 'Prenom': 'c'},
    'D': {'Nom': 'D', 'Prenom': 'd'},
    'E': {'Nom': 'E', 'Prenom': 'e'},
    'F': {'Nom': 'F', 'Prenom': 'f'},
    'G': {'Nom': 'G', 'Prenom': 'g'},
    'H': {'Nom': 'H', 'Prenom': 'h'},
    'I': {'Nom': 'I', 'Prenom': 'i'},
}

# Ajouter les attributs aux nœuds
infos = nx.set_node_attributes(G, nodes_attributes)

# Ajouter les arêtes avec les attributs spécifiés
edges = [
    ('A', 'I', {'color': 'blue', 'question': 'Q1'}),
    ('A', 'I', {'color': 'green', 'question': 'Q2'}),
    ('B', 'G', {'color': 'blue', 'question': 'Q1'}),
    ('B', 'C', {'color': 'blue', 'question': 'Q1'}),
    ('B', 'C', {'color': 'green', 'question': 'Q2'}),
    ('C', 'G', {'color': 'blue', 'question': 'Q1'}),
    ('C', 'H', {'color': 'green', 'question': 'Q2'}),
    ('C', 'I', {'color': 'black', 'question': 'Q4'}),
    ('D', 'G', {'color': 'blue', 'question': 'Q1'}),
    ('D', 'H', {'color': 'red', 'question': 'Q3'}),
    ('E', 'D', {'color': 'red', 'question': 'Q3'}),
    ('F', 'B', {'color': 'blue', 'question': 'Q1'}),
    ('F', 'G', {'color': 'blue', 'question': 'Q1'}),
    ('F', 'D', {'color': 'blue', 'question': 'Q1'}),
    ('F', 'B', {'color': 'green', 'question': 'Q2'}),
    ('F', 'G', {'color': 'green', 'question': 'Q2'}),
    ('G', 'B', {'color': 'blue', 'question': 'Q1'}),
    ('G', 'F', {'color': 'blue', 'question': 'Q1'}),
    ('G', 'F', {'color': 'green', 'question': 'Q2'}),
    ('G', 'B', {'color': 'red', 'question': 'Q3'}),
    ('H', 'D', {'color': 'red', 'question': 'Q3'}),
    ('H', 'D', {'color': 'black', 'question': 'Q4'}),
    ('I', 'A', {'color': 'blue', 'question': 'Q1'}),
    ('I', 'D', {'color': 'blue', 'question': 'Q1'}),
    ('I', 'A', {'color': 'green', 'question': 'Q2'}),
    ('I', 'C', {'color': 'black', 'question': 'Q4'}),
]

G.add_edges_from(edges)

# Créer le layout pour Dash Cytoscape
layout = {'name': 'circle'}

# Créer le style pour les arêtes avec des courbes et des flèches
edge_style = [
    {
        'selector': f'edge[color = "{color}"]',
        'style': {
            'line-color': color,
            'curve-style': 'bezier',  # Ajouter des courbes aux arêtes
            'target-arrow-shape': 'triangle',  # Ajouter des flèches
            'target-arrow-color': 'data(color)'
        }
    } for color in ['green', 'black', 'blue', 'red']
]

# Créer le style pour les nœuds avec des labels
node_style = [
    {
        'selector': 'node',
        'style': {
            'label': 'data(id)',  # Afficher le label du nœud (nom)
            'color': 'black'  # Couleur du texte du label
        }
    }
]

# Combiner les deux styles
style = edge_style + node_style

# Créer le composant Cytoscape pour Dash
cyto_compo = cyto.Cytoscape(
    id='cytoscape',
    elements=nx.cytoscape_data(G)['elements'],
    style={'width': '100%', 'height': '400px'},
    layout=layout,
    stylesheet=style
)

# Définir l'application Dash
app.layout = html.Div(
    children=[
        cyto_compo,
        html.H2(children="Informations sur les individus"),
        html.Div(id='node-info-output'),
        html.H2(children="Système de Regroupement"),

        html.Label("Nombre d'individus par groupes :"),
        dcc.Input(id='group-size-input-random', type='number', value=3, min=1, max=len(G.nodes)),
        html.Button("Regrouper aléatoirement", id="group-button-random"),
        html.Div(id="group-output-random"),

        html.Label("Nombre d'individus par groupes :"),
        dcc.Input(id='group-size-input-positif', type='number', value=3, min=1, max=len(G.nodes)),
        html.Button("Regrouper par préférences", id="group-button-positif"),
        html.Div(id="group-output-positif"),
    ],
)

# Callback pour afficher les informations des nœuds lors du clic
@app.callback(
    Output('node-info-output', 'children'),
    [Input('cytoscape', 'tapNodeData')]
)
def display_node_info(data):
    if data:
        # data = ID: {'id': 'I', 'value': 'I', 'name': 'I'}
        node_id = data['id']
        node_attribute = G.nodes[node_id]
        nom = node_attribute.get('Nom', 'N/A')
        prenom = node_attribute.get('Prenom', 'N/A')

        return html.Div([
            html.H4(f"ID: {data['id']} ::  Nom: {nom} : Prénom: {prenom}"),
        ])
    else:
        return html.Div("Cliquez sur un nœud pour afficher les informations.")



# Callback pour le système de regroupement random
@app.callback(
    Output("group-output-random", "children"),
    [Input("group-button-random", "n_clicks")],
    [Input("group-size-input-random", "value")]
)
def generate_random_groups(n_clicks, group_size):
    if n_clicks:
        # Liste des nœuds
        nodes_list = list(G.nodes)

        # Mélanger la liste des nœuds
        random.shuffle(nodes_list)

        # Diviser la liste en groupes de la taille spécifiée
        groups = [nodes_list[i:i + group_size] for i in range(0, len(nodes_list), group_size)]

        # Afficher les groupes
        group_output = [html.Div(f"Groupe {i + 1}: {group}") for i, group in enumerate(groups)]

        return group_output
    else:
        return html.Div("Cliquez sur le bouton pour générer des groupes aléatoires.")










# Callback pour le système de regroupement basé sur les préférences inter-individuelles
@app.callback(
    Output("group-output-positif", "children"),
    [Input("group-button-positif", "n_clicks")],
    [Input("group-size-input-positif", "value")]
)
def generate_preference_groups(n_clicks, group_size):
    if n_clicks:
        # Calculer les préférences inter-individuelles
        pref_indiv = {}
        for edge in G.edges(data=True):
            print(edge)
            node1, node2 = edge[0], edge[1]
            q_value = edge[2].get('question', '')
            weight = {'Q1': 2, 'Q2': 1, 'Q3': -2, 'Q4': -1}.get(q_value, 0)

            # Ajouter le poids à la paire de nœuds, en additionnant s'il existe déjà une pondération
            pref_indiv[(node1, node2)] = pref_indiv.get((node1, node2), 0) + weight

        print(pref_indiv)

        # Initialiser les groupes
        nodes_list = list(G.nodes)

        # Générer toutes les combinaisons de groupes possibles
        all_combinations = list(combinations(nodes_list, group_size))
        print(all_combinations)

        # Calculer la préférence cumulée pour chaque combinaison avec permutations
        pref_combinations = []
        for combination in all_combinations:
            cumulative_preference = 0
            for node1, node2 in permutations(combination, 2):
                cumulative_preference += pref_indiv.get((node1, node2), 0)
            pref_combinations.append((combination, cumulative_preference))

        # Écrire dans un fichier CSV
        with open('preferences_3.csv', 'w', newline='') as csvfile:
            csv_writer = csv.writer(csvfile)
            csv_writer.writerow(['Combinaison', 'Préférence Cumulée'])
            csv_writer.writerows([(str(combination), preference) for combination, preference in pref_combinations])


        # Trier pref_combinations en fonction de la préférence cumulée du plus grand au plus petit
        sorted_combinations = sorted(pref_combinations, key=lambda x: x[1], reverse=True)

        # Initialiser les groupes
        selected_groups = []
        remaining_nodes = set(nodes_list)

        # Déterminer le nombre d'individus par groupe
        individuals_per_group = group_size
        num_groups = len(G.nodes) // individuals_per_group

        # Construire les groupes homogènes
        for _ in range(num_groups):
            # Sélectionner le groupe avec la plus grande préférence cumulée
            current_group = sorted_combinations.pop(0)[0]

            # Vérifier s'il y a des individus identiques avec les groupes existants
            while any(set(current_group).intersection(set(group)) for group in selected_groups):
                current_group = sorted_combinations.pop(0)[0]

            # Ajouter le groupe sélectionné aux groupes finaux
            selected_groups.append(current_group)

            # Retirer les individus du groupe sélectionné des individus restants
            remaining_nodes -= set(current_group)

        # Traiter le cas des individus restants
        if remaining_nodes:
            # Créer un groupe unique avec les individus restants
            remaining_group = list(remaining_nodes)
            selected_groups.append(remaining_group)

        # Afficher les groupes
        group_output = [html.Div(f"Groupe {i + 1}: {group}") for i, group in enumerate(selected_groups)]

        return group_output

    else:
        return html.Div("Cliquez sur le bouton pour générer des groupes basés sur les préférences inter-individuelles.")





# Exécuter l'application
if __name__ == '__main__':
    app.run_server(debug=True)