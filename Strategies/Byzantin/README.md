# Problème des généraux byzantins



# Sources
[Wikipedia](https://fr.wikipedia.org/wiki/Probl%C3%A8me_des_g%C3%A9n%C3%A9raux_byzantins, "Article")  
[Mathematical Analysis and Algorithms for Federated Byzantine Agreement Systems, André Gaul, Ismail Khoffi, Jörg Liesen, and Torsten Stüber†, December 2019](https://www.researchgate.net/publication/337730065_Mathematical_Analysis_and_Algorithms_for_Federated_Byzantine_Agreement_Systems, "Article")  
[The Byzantine Generals Problem, LESLIE LAMPORT, ROBERT SHOSTAK, and MARSHALL PEASE, SRI International, November 1981](https://lamport.azurewebsites.net/pubs/byz.pdf, "Article")  


