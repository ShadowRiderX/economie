# Eye Tracking

>Travail effectué pour le compte d'un labo de recherche sur les comprortements des internautes.  
>La base de données, le protocole ainsi que les résultats de recherche n'étant pas libre ; Je ne partage ici que certains scripts simplifié...   

## Thématique
>Comprendre le comportement de navigation des inernautes.
>Faire passer un certain nombre d'individus en laboratoire appareillés avec un Eye Tracker durant leurs navigations.  
Il question de prendre connaissance des stimulis qui attire l'oeil des internautes durant leurs navigations...  

## Agrégation des données
>Les données brutes au formats .csv m'ont été rendus par le logiciel du laboratoire tel que l'on peut les voir dans le dossier: PARTICIPATE/[1;6]/AOI/[A;E]   
Dans chaque dossier d'un stimulus (A à E) un ensemble de fichier correspond aux sorties de données qu'il est nécéssaire d'agréger pour recontruire la navigation de l'internaute.  

## Scrapping
>Afin d'identifier sur quel partie du site ce trouve l'internaute et/ou quel mots a t-il tapé dans la barre de recherche pour se rendre sur une page en particulier ; j'ai réalisé un script de scrapping qui s'appuie sur les url renvoyés par le logiciel du laboratoire.  



