import pandas as pd

path_brute = f'../../../PARTICIPATE/{participate}/NAV/Brute'
path_result = f'../../../PARTICIPATE/{participate}/NAV/Result'

df_navigation = pd.read_csv(f'{path_result}/etape1_navigation_participate{participate}.csv', sep=";", encoding='utf8')
df_duration = pd.read_csv(f'{path_brute}/etape3_duree_participate{participate}.csv', sep=",", encoding='utf8')

N_KEYWORDS = df_navigation['KEYWORDS']
N_RECHERCHE = df_navigation['RECHERCHE']

NAVIGATION = N_KEYWORDS.combine_first(N_RECHERCHE)
NAVIGATION.to_csv(f'{path_result}/etape2_navigation_participate{participate}.csv', header=['NAVIGATION'], index=None)




