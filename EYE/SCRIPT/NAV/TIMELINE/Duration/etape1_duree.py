import pandas as pd
import numpy as np
import csv

path_brute = f'../../../../PARTICIPATE/{participate}/NAV/Brute'

df = pd.read_csv(f"{path_brute}/timeline_participate{participate}.csv", sep=";", encoding='utf8')


start = df['start']
end = df['end']

taleaux = []

MINUTES_START = []
SECONDES_START = []
for ligne_start in start:
    duration_start = ligne_start.split(':')
    minutes_start = duration_start[0]
    secondes_start = duration_start[1]
    MINUTES_START.append(duration_start[0])
    SECONDES_START.append(duration_start[1])

MINUTES_END = []
SECONDES_END = []
for ligne_end in end:
    duration_end = ligne_end.split(':')
    MINUTES_END.append(duration_end[0])
    SECONDES_END.append(duration_end[1])

taleaux.append([MINUTES_START, SECONDES_START, MINUTES_END, SECONDES_END])
print(taleaux[0])

T_tableaux = np.transpose(taleaux[0])

# Enregistre les deux listes de vocabulaire dans une fchier csv
with open(f"{path_brute}/etape1_duree_participate{participate}.csv", "w", encoding='utf-8') as file:
    data = csv.writer(file, delimiter=';', lineterminator='\n')
    data.writerow(['MINUTES_START', 'SECONDES_START', 'MINUTES_END', 'SECONDES_END'])
    for row in T_tableaux:
        data.writerow(row)

