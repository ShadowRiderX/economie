import pandas as pd
import numpy as np
import csv

path_brute = f'../../../../PARTICIPATE/{participate}/NAV/Brute'

df = pd.read_csv(f"{path_brute}/etape1_duree_participate{participate}.csv", sep=";", encoding='utf8')

TAB = []
MINUTES_START = df['MINUTES_START']
SECONDES_START = df['SECONDES_START']
MINUTES_END = df['MINUTES_END']
SECONDES_END = df['SECONDES_END']

MINUTES_DURATION = MINUTES_END - MINUTES_START
print(MINUTES_DURATION)

SECONDES_DURATION = SECONDES_END - SECONDES_START
print(SECONDES_DURATION)

TAB.append([MINUTES_DURATION, SECONDES_DURATION])
T_TAB = np.transpose(TAB[0])

# Enregistre les deux listes de vocabulaire dans une fchier csv
with open(f"{path_brute}/etape2_duree_participate{participate}.csv", "w", encoding='utf-8') as file:
    data = csv.writer(file, delimiter=';', lineterminator='\n')
    data.writerow(['MINUTES', 'SECONDES'])
    for row in T_TAB:
        data.writerow(row)



