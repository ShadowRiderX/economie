import pandas as pd
import numpy as np
import csv

path_brute = f'../../../../PARTICIPATE/{participate}/NAV/Brute'

df = pd.read_csv(f"{path_brute}/etape2_duree_participate{participate}.csv", sep=";", encoding='utf8')

MINUTES_s = df['MINUTES']*60
SECONDES_s = df['SECONDES']
DUREE = MINUTES_s + SECONDES_s

DUREE.to_csv(f'{path_brute}/etape3_duree_participate{participate}.csv', header=['DUREE'])



