import pandas as pd

path_result = f'../../../PARTICIPATE/{participate}/NAV/Result'
df = pd.read_csv(f'{path_result}/etape3_navigation_participate{participate}.csv', sep=";", encoding='utf8')

somme_data = df.groupby(['NUM', 'NAVIGATION'])['DUREE'].sum()
somme_data.to_csv(f'{path_result}/etape4_navigation_participate{participate}.csv')





