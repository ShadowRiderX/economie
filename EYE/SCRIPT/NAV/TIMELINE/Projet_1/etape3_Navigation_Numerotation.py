import pandas as pd
import csv

path_brute = f'../../../PARTICIPATE/{participate}/NAV/Brute'
path_result = f'../../../PARTICIPATE/{participate}/NAV/Result'

df_navigation = pd.read_csv(f'{path_result}/etape2_navigation_participate{participate}.csv', sep=",", encoding='utf8')
df_duration = pd.read_csv(f'{path_brute}/etape3_duree_participate{participate}.csv', sep=",", encoding='utf8')

df_navigation = df_navigation.fillna('Autre')

LIST_LABEL = [['NUM', 'NAVIGATION', 'DUREE']]

df_length = df_navigation.shape[0]

num = 0
k = 0
for i in range(0, df_length):
    navigation = df_navigation['NAVIGATION'][i]
    duree = df_duration['DUREE'][i]
    a = df_navigation.iloc[k].values
    b = df_navigation.iloc[k-1].values
    if a.any() == b.any():
        num = num
        print(num)
        LIST_LABEL.append([num, navigation, duree])
    else:
        num = num + 1
        print(num)
        LIST_LABEL.append([num, navigation, duree])
    k = k + 1

with open(f"{path_result}/etape3_navigation_participate{participate}.csv", "w", encoding='utf-8') as file_voc:
    data = csv.writer(file_voc, delimiter=';', lineterminator='\n')
    for row in LIST_LABEL:
        data.writerow(row)






