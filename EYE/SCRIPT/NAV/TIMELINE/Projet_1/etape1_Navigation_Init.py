import pandas as pd
import re
import csv


path_brute = f'../../../PARTICIPATE/{participate}/NAV/Brute'
path_result = f'../../../PARTICIPATE/{participate}/NAV/Result'

df_navigation = pd.read_csv(f"{path_brute}/timeline_participate{participate}.csv", sep=";", encoding='utf8')

# Sélectionné la colonne url
url = df_navigation['url']

LIST_URL = []
for element_df in url:
    element_df = re.sub('[0-9]', '', element_df)
    element_df = re.sub('\|', '', element_df)
    LIST_URL.append(element_df)

LIST_LABEL = [['KEYWORDS', 'RECHERCHE', 'DUREE']]

for element_url in LIST_URL:
    partern_keywords = 'keywords=(.[a-z-]{1,70}[^#/$&+%])'
    Rex_KEYWORDS = re.findall(partern_keywords, element_url)
    keywords = ''
    if len(Rex_KEYWORDS) == 1:
        keywords = Rex_KEYWORDS[0]

    partern_recherche = 'recherche.{1,2}text=(.[a-zA-Z\xc0-\xd6\xd8-\xf6\xf8-\xff\s]{1,70}[^#/$&+%])'
    Rex_RECHERCHE = re.findall(partern_recherche, element_url)
    recherche = ''
    if len(Rex_RECHERCHE) == 1:
        recherche = Rex_RECHERCHE[0]

    LIST_LABEL.append([keywords, recherche])

# Enregistre les deux listes de vocabulaire dans une fchier csv
with open(f"{path_result}/etape1_navigation_participate{participate}.csv", "w", encoding='utf-8') as file:
    data = csv.writer(file, delimiter=';', lineterminator='\n')
    for row in LIST_LABEL:
        data.writerow(row)
        
        
        

