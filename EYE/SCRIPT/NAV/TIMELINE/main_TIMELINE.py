Project = '1'
globlsparam = {'participate': 1}

if Project == '1':
    exec(open("Projet_1/etape1_Navigation_Init.py").read(), globlsparam)
    exec(open("Projet_1/etape2_Navigation_Combine.py").read(), globlsparam)
    exec(open("Projet_1/etape3_Navigation_Numerotation.py").read(), globlsparam)
    exec(open("Projet_1/etape4_Navigation_Finale.py").read(), globlsparam)
    exec(open("Projet_1/TimeLine_Voc.py").read(), globlsparam)

elif Project == '2':
    exec(open("Projet_2/etape1_Navigation_Init.py").read(), globlsparam)
    exec(open("Projet_2/etape2_Navigation_Combine.py").read(), globlsparam)
    exec(open("Projet_2/etape3_Navigation_Numerotation.py").read(), globlsparam)
    exec(open("Projet_2/etape4_Navigation_Finale.py").read(), globlsparam)
    exec(open("Projet_2/TimeLine_Voc.py").read(), globlsparam)


