import pandas as pd
import glob
import os
import functools

path_agreg = f'../../PARTICIPATE/{participate}/AOI/AGREG'


files_joined_page = os.path.join(f'../../PARTICIPATE/{participate}/AOI/{stimulus}/', "*.csv")
list_files_page = glob.glob(files_joined_page)
dataframe = pd.concat(map(functools.partial(pd.read_csv, sep=';'), list_files_page))
dataframe.to_csv(f'{path_agreg}/AGREG_participate{participate}_stimulus{stimulus}.csv')




