import pandas as pd

path_agreg = f'../../PARTICIPATE/{participate}/AOI/AGREG'
path_result = f'../../PARTICIPATE/{participate}/AOI/RESULT'

df_init = pd.read_csv(f'{path_agreg}/REPLACE_participate{participate}_stimulus{stimulus}.csv', sep=",", encoding='utf8')
somme = df_init.groupby(['AOI'])['Number of fixations', 'Duration of all fixations in ms', 'Number of visits'].sum()
somme.to_csv(f'{path_result}/RESULT_participate{participate}_stimulus{stimulus}.csv')





