import pandas as pd
import glob
import os

path_agreg = f'../../PARTICIPATE/{participate}/AOI/AGREG'

df_init = pd.read_csv(f'{path_agreg}/AGREG_participate{participate}_stimulus{stimulus}.csv', sep=",", encoding='utf8')
df_init['Duration of all fixations in ms'] = df_init['Duration of all fixations in ms'].str.replace(" ms", "")
df_init.to_csv(f'{path_agreg}/REPLACE_participate{participate}_stimulus{stimulus}.csv')


