# Prediction des prix du Bitcoin

## Chargement des librairies
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, LSTM

## Chargement des données
data = pd.read_csv("Bitcoin.csv")
print(data.head())
print(data.info())

data['Ouv'] = [float(str(i).replace(",", "")) for i in data['Ouv']]

scaler = MinMaxScaler(feature_range=(0,1))
scaled_data = scaler.fit_transform(data['Ouv'].values.reshape(-1,1))

prediction_days = 60


## Construction du modèle
x_train = []
y_train = []

for x in range(prediction_days, len(scaled_data)):
    x_train.append(scaled_data[x-prediction_days:x, 0])
    y_train.append(scaled_data[x, 0])
    
x_train, y_train = np.array(x_train), np.array(y_train)
x_train = np.reshape(x_train, (x_train.shape[0], x_train.shape[1], 1))

model = Sequential()

model.add(LSTM(units=50, return_sequences=True, input_shape=(x_train.shape[1], 1)))
model.add(Dropout(0.2))
model.add(LSTM(units=50, return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(units=50))
model.add(Dropout(0.2))
model.add(Dense(units=1))

model.compile(optimizer='adam', loss='mean_squared_error')
model.fit(x_train, y_train, epochs=25, batch_size=32)


## dataset de test
test_data = pd.read_csv("Bitcoin_test.csv")
test_data['Ouv'] = [float(str(i).replace(",", "")) for i in test_data['Ouv']]

actual_prices = test_data['Ouv'].values

total_dataset = pd.concat((data['Ouv'], test_data['Ouv']), axis=0)

model_inputs = total_dataset[len(total_dataset) - len(test_data) - prediction_days:].values
model_inputs = model_inputs.reshape(-1, 1)
model_inputs = scaler.transform(model_inputs)


## Prediction sur sur le dataset de test
x_test = []

for x in range(prediction_days, len(model_inputs)):
    x_test.append(model_inputs[x-prediction_days:x, 0])
    
x_test = np.array(x_test)
x_test = np.reshape(x_test, (x_test.shape[0], x_test.shape[1], 1))

predicted_prices = model.predict(x_test)
predicted_prices = scaler.inverse_transform(predicted_prices)

## Graphique des prediction de prix avec le dataset de test
plt.plot(actual_prices, color='black', label=f"Prix actuel du Bitcoin")
plt.plot(predicted_prices, color='green', label=f"Prix du ​​Bitcoin prédits")
plt.title(f"Prix des actions Bitcoin")
plt.xlabel("Dates")
plt.ylabel(f"Prix des actions bitcoin")
plt.legend()
plt.show()


## Prediction sur les prix future
real_data = [model_inputs[len(model_inputs) + 1 - prediction_days:len(model_inputs + 1), 0]]
real_data = np.array(real_data)
real_data = np.reshape(real_data, (real_data.shape[0], real_data.shape[1], 1))

prediction = model.predict(real_data)
prediction = scaler.inverse_transform(prediction)
print(f"Prediction : {prediction}")






## Prediction sur les 100 prix future
L_prediction = []
counter = 1

for predict_value in range(1,100):
    real_data = [model_inputs[len(model_inputs) + counter - prediction_days:len(model_inputs + counter), 0]]
    real_data = np.array(real_data)
    real_data = np.reshape(real_data, (real_data.shape[0], real_data.shape[1], 1))

    prediction = model.predict(real_data)
    prediction = scaler.inverse_transform(prediction)
    L_prediction.append(prediction)
    counter += 1
    
print(L_prediction)


plt.plot(actual_prices, color='black', label=f"Prix actuel du Bitcoin")
plt.plot(L_prediction, color='green', label=f"Prix du ​​Bitcoin prédits")
plt.title(f"Prix des actions Bitcoin")
plt.xlabel("Dates")
plt.ylabel(f"Prix des actions bitcoin")
plt.legend()
plt.show()

# plt.plot(actual_prices, color='black', label=f"Prix actuel du Bitcoin")
# labels = data['Date']
# plt.xticks(LL_prediction, labels, rotation='vertical')








