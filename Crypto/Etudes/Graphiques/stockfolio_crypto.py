import datetime as dt
import investpy
import matplotlib.pyplot as plt

tickers = ['Bitcoin', 'Ethereum', 'Litecoin', 'XRP', 'DASH', 'Siacoin']
amounts = [7, 45, 12, 16, 2, 4]
prices = []
total = []
start = dt.datetime(2014, 1, 1).strftime("%d/%m/%Y")
end = dt.datetime.now().strftime("%d/%m/%Y")

for ticker in tickers:
    df = investpy.get_crypto_historical_data(crypto=str(ticker),
                                           from_date=start,
                                           to_date=end)
    price = df[-1:]['Close'][0]
    prices.append(price)
    index = tickers.index(ticker)
    total.append(price * amounts[index])

fig, ax = plt.subplots(figsize=(16, 8))

ax.set_facecolor('black')
ax.figure.set_facecolor('#121212')

ax.tick_params(axis='x', color='white')
ax.tick_params(axis='y', color='white')

ax.set_title('Portefeuille', color="#EF6C35", fontsize=20)

_, texts, _ = ax.pie(total, labels=tickers, autopct="%1.1f%%", pctdistance=0.8)
[text.set_color('white') for text in texts]

my_circle = plt.Circle((0, 0), 0.55, color='black')
plt.gca().add_artist(my_circle)

ax.text(-2, 1, 'Aperçu du Portefeuille', fontsize=14, color="#EF6C35", verticalalignment='center', horizontalalignment='center')
ax.text(-2, 0.85, f'Montant Total USD: {sum(total):.2f} $', fontsize=12, color='white', verticalalignment='center', horizontalalignment='center')
counter = 0.15
for ticker in tickers:
    ax.text(-2, 0.85 - counter, f'{ticker}: {total[tickers.index(ticker)]:.2f} $', fontsize=12, color='white', verticalalignment='center', horizontalalignment='center')
    counter += 0.15

plt.show()



