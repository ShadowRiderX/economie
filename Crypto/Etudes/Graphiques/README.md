# SCRIPT DE TEST


>Librairies utilisés
* [pandas](https://pandas.pydata.org/docs/ "pandas")  
* [matplotlibs](https://matplotlib.org/ "matplotlib")  
* [investpy](https://investpy.readthedocs.io/ "investpy")  
* [mplfinance](https://github.com/matplotlib/mplfinance "mplfinance")  
* [datetime](https://docs.python.org/fr/3/library/datetime.html "datetime")  


## Graphiques
* candlestick_chart.py
>Affiche les cours de cloture de l'Ethereum.

* StockAnalysisRSI.py
>Affiche le Relative Strength Index (RSI) de l'Ethereum.

* stockfolio_crypto.py
>Affiche un stockfolio fictif de plusieurs crypto.


## Scraping


