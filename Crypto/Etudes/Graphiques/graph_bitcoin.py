import plotly.express as px
import investpy
import datetime as dt

currency = "USD"
metric = "Close"
start = dt.datetime(2010, 1, 1).strftime("%d/%m/%Y")
end = dt.datetime.now().strftime("%d/%m/%Y")

XX = 'Open'
YY = 'Close'
crypto = ['Bitcoin']
colnames = []
first = True

for ticker in crypto:
    data = investpy.get_crypto_historical_data(crypto=str(ticker), from_date=start, to_date=end)
    if first:
        combined = data[[metric]].copy()
        colnames.append(ticker)
        combined.columns = colnames
        first = False
    else:
        combined = combined.join(data[metric])
        colnames.append(ticker)
        combined.columns = colnames

fig = px.line(combined)
'''fig.update_yaxes(type="log")'''
'''title='Historiques des clotures du Bitcoin','''
fig.update_layout(xaxis_title='Années', yaxis_title='Clotures', showlegend=False)
fig.update_xaxes(
    rangeslider_visible=True,
    rangeselector=dict(
        buttons=list([
            dict(count=1, label="1m", step="month", stepmode="backward"),
            dict(count=6, label="6m", step="month", stepmode="backward"),
            dict(count=1, label="YTD", step="year", stepmode="todate"),
            dict(count=1, label="1y", step="year", stepmode="backward"),
            dict(count=2, label="2y", step="year", stepmode="backward"),
            dict(count=3, label="3y", step="year", stepmode="backward"),
            dict(count=5, label="5y", step="year", stepmode="backward"),
            dict(step="all")
        ])
    )
)
'''plotly.offline.plot(fig, filename='pageBitcoin.html', config={'displayModeBar': False})'''
'''plotly.offline.plot(fig, filename='pageBitcoin.html', config={'displayModeBar': True})'''
fig.show()





