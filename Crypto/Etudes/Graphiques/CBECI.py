import plotly.express as px
import pandas as pd
import plotly

df = pd.read_csv('data/cbeci.csv')

fig = px.line(df, x='Date and Time', y=df.columns[2:5], title='Historical Bitcoin Network power demand')

fig.update_xaxes(
    rangeslider_visible=True,
    rangeselector=dict(
        buttons=list([
            dict(count=1, label="1m", step="month", stepmode="backward"),
            dict(count=6, label="6m", step="month", stepmode="backward"),
            dict(count=1, label="YTD", step="year", stepmode="todate"),
            dict(count=1, label="1y", step="year", stepmode="backward"),
            dict(step="all")
        ])
    )
)
plotly.offline.plot(fig, filename='cbeci.html', config={'displayModeBar': True})
fig.show()
