# Chargement des librairies
import datetime as dt
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import investpy
from mplfinance.original_flavor import candlestick_ohlc

# Definition du datetime
start = dt.datetime(2017, 1, 1).strftime("%d/%m/%Y")
end = dt.datetime.now().strftime("%d/%m/%Y")

# Chargement de la base de donnée
cryptolist = 'Ethereum'
data = investpy.get_crypto_historical_data(crypto=str(cryptolist), from_date=start, to_date=end)

# Structure des données
data = data[['Open', 'High', 'Low', 'Close']]
data.reset_index(inplace=True)
data['Date'] = data['Date'].map(mdates.date2num)
print(data.head())

# Graphique
ax = plt.subplot()
ax.grid(True)
ax.set_axisbelow(True)
ax.set_title('{} prix'.format(cryptolist), color='white')
ax.set_facecolor('black')
ax.figure.set_facecolor('#121212')
ax.tick_params(axis='x', colors='white')
ax.tick_params(axis='y', colors='white')
ax.xaxis_date()

candlestick_ohlc(ax, data.values, width=0.5, colorup='#00ff00')
plt.show()

