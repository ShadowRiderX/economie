# Prévision de séries chronologiques du Bitcoin

import pandas_datareader.data as web
import datetime
import pandas as pd

## Paramètres

pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)

## Chargement du dataset

data = pd.read_csv("db_Bitcoin.csv")
print(data.head())
print(data.info())

data.index = pd.to_datetime(data['Date'], format='%d/%m/%Y')

import matplotlib.pyplot as plt

graph = data['Close']
combined = pd.DataFrame()
combined['Close'] = data['Close']

plt.figure(figsize=(12, 8))
ax1 = plt.subplot(211)
ax1.plot(combined.index, combined['Close'], color='lightgrey')
ax1.set_title("Close BTC", color='white')

ax1.grid(True, color='#555555')
ax1.set_axisbelow(True)
ax1.set_facecolor('black')
ax1.figure.set_facecolor('#121212')
ax1.tick_params(axis='x', colors='white')
ax1.tick_params(axis='y', colors='white')

plt.show()

train = data[data.index < pd.to_datetime("01/01/2021", format='%d/%m/%Y')]
test = data[data.index > pd.to_datetime("01/01/2021", format='%d/%m/%Y')]

graph = data['Close']
combined_train, combined_test = pd.DataFrame(), pd.DataFrame()
combined_train['Close'] = train['Close']
combined_test['Close'] = test['Close']

plt.figure(figsize=(12, 8))
ax1 = plt.subplot(211)

ax1.plot(combined_train.index, combined_train['Close'], color='lightgrey')
ax1.plot(combined_test.index, combined_test['Close'], color='red')

ax1.set_title("Close BTC", color='white')
ax1.grid(True, color='#555555')
ax1.set_axisbelow(True)
ax1.set_facecolor('black')
ax1.figure.set_facecolor('#121212')
ax1.tick_params(axis='x', colors='white')
ax1.tick_params(axis='y', colors='white')

plt.show()

## Moyenne mobile autorégressive (ARMA)

from statsmodels.tsa.statespace.sarimax import SARIMAX

y = train['Close']

ARMAmodel = SARIMAX(y, order = (1, 0, 1))
ARMAmodel = ARMAmodel.fit()

y_pred_arma = ARMAmodel.get_forecast(len(test.index))
y_pred_arma_df = y_pred_arma.conf_int(alpha = 0.05) 
y_pred_arma_df["Predictions"] = ARMAmodel.predict(start=y_pred_arma_df.index[0], end=y_pred_arma_df.index[-1])
y_pred_arma_df.index = test.index
y_pred_out_arma = y_pred_arma_df["Predictions"]

plt.plot(y_pred_out_arma, color='green', label = 'Predictions')
plt.legend()

graph = data['Close']
combined_train, combined_test = pd.DataFrame(), pd.DataFrame()
combined_train['Close'] = train['Close']
combined_test['Close'] = test['Close']

plt.figure(figsize=(12, 8))
ax1 = plt.subplot(211)

ax1.plot(combined_train.index, combined_train['Close'], color='lightgrey')
ax1.plot(combined_test.index, combined_test['Close'], color='red')
ax1.plot(y_pred_out_arma, color='green', label = 'ARMA Predictions')

ax1.set_title("Close BTC", color='white')
ax1.grid(True, color='#555555')
ax1.set_axisbelow(True)
ax1.set_facecolor('black')
ax1.figure.set_facecolor('#121212')
ax1.tick_params(axis='x', colors='white')
ax1.tick_params(axis='y', colors='white')

plt.legend()
plt.show()

import numpy as np
from sklearn.metrics import mean_squared_error

arma_rmse = np.sqrt(mean_squared_error(test["Close"].values, y_pred_arma_df["Predictions"]))
print("RMSE: ",arma_rmse)

## Moyenne mobile intégrée autorégressive (ARIMA)

from statsmodels.tsa.arima.model import ARIMA

ARIMAmodel = ARIMA(y, order = (2, 2, 2))
ARIMAmodel = ARIMAmodel.fit()

graph = data['Close']
combined_train, combined_test = pd.DataFrame(), pd.DataFrame()
combined_train['Close'] = train['Close']
combined_test['Close'] = test['Close']

plt.figure(figsize=(12, 8))
ax1 = plt.subplot(211)

ax1.plot(combined_train.index, combined_train['Close'], color='lightgrey', label='Train')
ax1.plot(combined_test.index, combined_test['Close'], color='red', label='Test')
ax1.plot(y_pred_out_arma, color='green', label='ARMA Predictions')

y_pred_arima = ARIMAmodel.get_forecast(len(test.index))
y_pred_arima_df = y_pred_arima.conf_int(alpha = 0.05) 
y_pred_arima_df["Predictions"] = ARIMAmodel.predict(start=y_pred_arima_df.index[0], end=y_pred_arima_df.index[-1])
y_pred_arima_df.index = test.index
y_pred_out_arima = y_pred_arima_df["Predictions"] 
ax1.plot(y_pred_out_arima, color='Yellow', label='ARIMA Predictions')

ax1.set_title("Close BTC", color='white')
ax1.grid(True, color='#555555')
ax1.set_axisbelow(True)
ax1.set_facecolor('black')
ax1.figure.set_facecolor('#121212')
ax1.tick_params(axis='x', colors='white')
ax1.tick_params(axis='y', colors='white')

plt.legend()
plt.show()

import numpy as np
from sklearn.metrics import mean_squared_error

arima_rmse = np.sqrt(mean_squared_error(test["Close"].values, y_pred_arima_df["Predictions"]))
print("RMSE: ",arima_rmse)

## ARIMA saisonnier (SARIMA)

SARIMAXmodel = SARIMAX(y, order=(1, 0, 0), seasonal_order=(2,2,2,12))
SARIMAXmodel = SARIMAXmodel.fit()

combined_train, combined_test = pd.DataFrame(), pd.DataFrame()
combined_train['Close'] = train['Close']
combined_test['Close'] = test['Close']

plt.figure(figsize=(12, 8))
ax1 = plt.subplot(211)

ax1.plot(combined_train.index, combined_train['Close'], color='lightgrey', label='Train')
ax1.plot(combined_test.index, combined_test['Close'], color='red', label='Test')

y_pred_arma = ARMAmodel.get_forecast(len(test.index))
y_pred_arma_df = y_pred_arma.conf_int(alpha = 0.05) 
y_pred_arma_df["Predictions"] = ARMAmodel.predict(start=y_pred_arma_df.index[0], end=y_pred_arma_df.index[-1])
y_pred_arma_df.index = test.index
y_pred_out_arma = y_pred_arma_df["Predictions"] 
ax1.plot(y_pred_out_arma, color='green', label='ARMA Predictions')

y_pred_arima = ARIMAmodel.get_forecast(len(test.index))
y_pred_arima_df = y_pred_arima.conf_int(alpha = 0.05) 
y_pred_arima_df["Predictions"] = ARIMAmodel.predict(start=y_pred_arima_df.index[0], end=y_pred_arima_df.index[-1])
y_pred_arima_df.index = test.index
y_pred_out_arima = y_pred_arima_df["Predictions"] 
ax1.plot(y_pred_out_arima, color='Yellow', label='ARIMA Predictions')

y_pred_sarima = SARIMAXmodel.get_forecast(len(test.index))
y_pred_sarima_df = y_pred_sarima.conf_int(alpha = 0.05) 
y_pred_sarima_df["Predictions"] = SARIMAXmodel.predict(start=y_pred_sarima_df.index[0], end=y_pred_sarima_df.index[-1])
y_pred_sarima_df.index = test.index
y_pred_out_sarima = y_pred_sarima_df["Predictions"] 
ax1.plot(y_pred_out_sarima, color='Blue', label='SARIMA Predictions')

ax1.set_title("Close BTC", color='white')
ax1.grid(True, color='#555555')
ax1.set_axisbelow(True)
ax1.set_facecolor('black')
ax1.figure.set_facecolor('#121212')
ax1.tick_params(axis='x', colors='white')
ax1.tick_params(axis='y', colors='white')

plt.legend()
plt.show()


