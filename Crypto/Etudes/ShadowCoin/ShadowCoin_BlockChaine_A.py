import hashlib

class ShadowCoinBlock:
    def __init__(self, previous_block_hash, transaction_list):
        self.previous_block_hash = previous_block_hash
        self.transaction_list = transaction_list

        self.block_data = "-".join(transaction_list) + "-" + previous_block_hash
        self.block_hash = hashlib.sha256(self.block_data.encode()).hexdigest()

t1 = "Alice envoie 2.2 SC à Bob"
'''t2 = "Alexis envoie 4.5 SC à Shadow"'''
t2 = "Alexis envoie 2.6 SC à Shadow"
t3 = "Mallory envoie 3.9 SC à Shadow"
t4 = "Alexis envoie 3.3 SC à Alice"
t5 = "Bob envoie 1 SC à Charlie"
t6 = "Charlie envoie 1.4 SC à Mallory"

initial_block = ShadowCoinBlock("ChaineInitial", [t1, t2])

print(initial_block.block_data)
print(initial_block.block_hash)

''' 
Résultats Si t2 = "Alexis envoie 4.5 SC à Shadow"
Alice envoie 2.2 SC à Bob-Alexis envoie 4.5 SC à Shadow-ChaineInitial
3e8e3ee4e9bb1a865c4b7e8e1e1b5454b44cb2d17125ea5243bfc07b1f6da81a

Si l'on change une transaction 
exemple : t2 = "Alexis envoie 2.6 SC à Shadow"

Résultats Si t2 = "Alexis envoie 2.6 SC à Shadow"
Alice envoie 2.2 SC à Bob-Alexis envoie 2.6 SC à Shadow-ChaineInitial
371cfb512554d9be3e630a72b80defbcfb95f6cb5954a90713f1e0694cc7566f

Le hashage est totalement différents
Si nous répétons le même code sans rien changer ; l'output de hachage reste identique
Si nous réinitialison les informatons des transactions : l'output de hachage redevient identique au premier resultat
'''

print("\n ############################################## \n")

second_block = ShadowCoinBlock(initial_block.block_hash, [t3, t4])

print(second_block.block_data)
print(second_block.block_hash)

''' 
Résultats Si t2 = "Alexis envoie 4.5 SC à Shadow"
Mallory envoie 3.9 SC à Shadow-Alexis envoie 3.3 SC à Alice-3e8e3ee4e9bb1a865c4b7e8e1e1b5454b44cb2d17125ea5243bfc07b1f6da81a
2693c5bd3be4643556e4cbab3b0428e0b9ed31ccfa3cea9c72fb653a9432e1c5

Résultats Si t2 = "Alexis envoie 2.6 SC à Shadow"
Mallory envoie 3.9 SC à Shadow-Alexis envoie 3.3 SC à Alice-371cfb512554d9be3e630a72b80defbcfb95f6cb5954a90713f1e0694cc7566f
cf80033b0272e2d12f3fff6d8c342dedb5090f0427aacf47b41ecb1f6d5e98e3
'''

print("\n ############################################## \n")

third_block = ShadowCoinBlock(second_block.block_hash, [t5, t6])

print(third_block.block_data)
print(third_block.block_hash)

'''
Résultats Si t2 = "Alexis envoie 4.5 SC à Shadow"
Bob envoie 1 SC à Charlie-Charlie envoie 1.4 SC à Mallory-2693c5bd3be4643556e4cbab3b0428e0b9ed31ccfa3cea9c72fb653a9432e1c5
c63bf0a8fdc0786a9b1e6e7b460b2e77e8edce460388d125bb9e4b0123c27d43

Résultats Si t2 = "Alexis envoie 2.6 SC à Shadow"
Bob envoie 1 SC à Charlie-Charlie envoie 1.4 SC à Mallory-cf80033b0272e2d12f3fff6d8c342dedb5090f0427aacf47b41ecb1f6d5e98e3
1182674096f984465cf17360c89957f39b5aaba8859672b5c906adbd35fd91cf

Les changements se répercutent sur l'ensemble des transactions.
De cela, chacun peut vérifier l'intégrité de la chaine.
Si celle-ci ne correspond pas alors la transaction serait reffusé...
'''











