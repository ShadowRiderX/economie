# ShadowCoin (SC)

## Création d'une BlockChaine
>ShadowCoin_BlockChaine_A.py  

### Transcation
* tn = transcation  
* t1 = "Alice envoie 2.2 SC à Bob"  
* t2 = "Alexis envoie 4.5 SC à Shadow"  
* t3 = "Mallory envoie 3.9 SC à Shadow"  

>Ces transactions peuvent être stocké dans des blocks  

### Block initial
* Bn = Block  
* B1 : Le block initial à les informations sur les 3 premières transactions à ceci près qu'il à également lui même une base (ex : AAA)   
* B1 = ("AAA", t1, t2 ,t3)   

>Une fois le block constitué : on le hash et entraine donc une sortie (output) en plusieur partie  

### Principe du hachage
>L'algorithme de hashage utilisé est : SHA256  

* B1 = ("AAA", t1, t2 ,t3)   
* B1_bis1 = ("AAA", t3) et B1_bis2 = (t1, t2)  

ou encore  

* B1 = ("AAA", t1, t2 ,t3)  
* B1_bis1 = ("1Partie de AAA", "1Partie de t3")  
* B1_bis2 = ("1Partie de t1", "1Partie de t2")  
* B1_bis3 = ("1AutrePartie de t1", "1AutrePartie de AAA")  
* B1_bis4 = ("1AutrePartie de t2", "1AutrePartie de t3")  

etc...  

* B1 = ("AAA", t1, t2 ,t3) renvoie par exemple 76fd89  
76fd89 : correspond à ce que l'on verrais à l'output  

>ici c'est le résultat du hashage du 1er bloc (c'est un chiffre hexadécimal)  

>Nous alons également hacher le code... et pas seulement les transactions...  

>Maintenant un 2ème block est crée :  
* B2 qui reçois des informations du 1er block (l'output héxadécimale) et des informatons sur des transcations suplémentaires  
* B2 = (76fd89, t4, t5, t6...) renvoie par exemple 8923ff  
* B2 seras également haché et l'output servira de base au prochain block ainsi de suite...  

### Intégrité de la chaine
>Avec le hachage, il est difficile de revenir en arrière pour modifier une transaction.  
Si en t1 : Alice avait envoyé 2.1 SC à Bob et non 2.2 alors la sortie B1 = ("AAA", t1, t2 ,t3) renvoie par exemple fa56bb et non pas 76fd89  
Ainsi les output des autres block en aurait été également impacté...  
C'est toute l'intégrité de la chaine qui est impacté...  
Il faut recrée une transaction tn pour "annuler" la t1 MAIS les informations sur la t1 sont concervé !  

>Pour vérifier l'intégrité de la chaine, nous ne somme pas obligé de regarder tout les output
mais seulement le dernier puisque si la chaine avais été modifier le dernier output en serais différents...  
Si une transaction est modifier on peut facilement s'en apercevoir car il y aurais des dissimilarités qui invaliderais le processus de vérification.  





