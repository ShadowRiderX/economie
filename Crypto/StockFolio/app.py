from flask import Flask, render_template, request, url_for, redirect, flash
from flask_sqlalchemy import SQLAlchemy
import os

import datetime as dt
import investpy
import matplotlib.pyplot as plt
import base64
from io import BytesIO

import sqlite3 as sq


basedir = os.path.abspath(os.path.dirname(__file__))
app = Flask(__name__)
app.secret_key = "aaxxcleaipxw88_CRYPTO"

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db_portfolio.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SECRET_KEY'] = 'JSHJHDJJJCJNCIEPZMLSKSC'

db = SQLAlchemy(app)
db.create_all()

class Addcrypto(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name_crypto = db.Column(db.String(80), nullable=False)
    quantity_crypto = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return f'<Addcrypto {self.name_crypto}>'


@app.route('/')
def index():
    visible_crypto = Addcrypto.query.all()
    return render_template("stockfolio.html", visible_crypto=visible_crypto)


@app.route('/create/', methods=('GET', 'POST'))
def create():
    if request.method == 'POST':
        name_crypto = request.form['name_crypto']
        quantity_crypto = request.form['quantity_crypto']
        addcrypto = Addcrypto(name_crypto=name_crypto, quantity_crypto=quantity_crypto)
        db.session.add(addcrypto)
        db.session.commit()

        return redirect(url_for('index'))

    return render_template('create.html')


@app.route('/<int:crypto_id>/edit/', methods=('GET', 'POST'))
def edit(crypto_id):
    addcrypto = Addcrypto.query.get_or_404(crypto_id)

    if request.method == 'POST':
        name_crypto = request.form['name_crypto']
        quantity_crypto = request.form['quantity_crypto']

        addcrypto.name_crypto = name_crypto
        addcrypto.quantity_crypto = quantity_crypto

        db.session.add(addcrypto)
        db.session.commit()

        return redirect(url_for('index'))

    return render_template('edit.html', addcrypto=addcrypto)


@app.post('/<int:crypto_id>/delete/')
def delete(crypto_id):
    addcrypto = Addcrypto.query.get_or_404(crypto_id)
    db.session.delete(addcrypto)
    db.session.commit()

    return redirect(url_for('index'))


@app.route('/graph/')
def graph():
    conn = sq.connect("db_portfolio.db")
    c = conn.cursor()
    c.execute("SELECT * FROM addcrypto")
    items = c.fetchall()

    print(items)

    LIST_CRYPTO = []
    LIST_QUANTITY = []
    for item in items:
        LIST_CRYPTO.append(item[1])
        LIST_QUANTITY.append(item[2])

    print(LIST_CRYPTO)
    print(LIST_QUANTITY)

    conn.close()

    prices = []
    total = []
    start = dt.datetime(2014, 1, 1).strftime("%d/%m/%Y")
    end = dt.datetime.now().strftime("%d/%m/%Y")

    for CRYPTO in LIST_CRYPTO:
        df = investpy.get_crypto_historical_data(crypto=str(CRYPTO), from_date=start, to_date=end)
        price = df[-1:]['Close'][0]
        prices.append(price)
        index = LIST_CRYPTO.index(CRYPTO)
        total.append(price * LIST_QUANTITY[index])

    fig, ax = plt.subplots(figsize=(15, 7))

    ax.set_facecolor('black')
    ax.figure.set_facecolor('#121212')

    ax.tick_params(axis='x', color='white')
    ax.tick_params(axis='y', color='white')

    ax.set_title('Portfolio', color="#EF6C35", fontsize=20)

    _, texts, _ = ax.pie(total, labels=LIST_CRYPTO, autopct="%1.1f%%", pctdistance=0.8)
    [text.set_color('white') for text in texts]

    my_circle = plt.Circle((0, 0), 0.55, color='black')
    plt.gca().add_artist(my_circle)

    ax.text(-2, 1, 'Portfolio overview', fontsize=14, color="#EF6C35", verticalalignment='center',
            horizontalalignment='center')
    ax.text(-2, 0.85, f'Total USD Amount: {sum(total):.2f} $', fontsize=12, color='white', verticalalignment='center',
            horizontalalignment='center')
    counter = 0.15

    for CRYPTO in LIST_CRYPTO:
        ax.text(-2, 0.85 - counter, f'{CRYPTO}: {total[LIST_CRYPTO.index(CRYPTO)]:.2f} $', fontsize=12, color='white',
                verticalalignment='center', horizontalalignment='center')
        counter += 0.15

    buf = BytesIO()
    fig.savefig(buf, format="png")
    data = base64.b64encode(buf.getbuffer()).decode("ascii")
    return f'''
        <link rel="stylesheet" type="text/css" href="static/css/style.css" />
        <img src="data:image/png;base64,{data}"/>
        '''


if __name__ == "__main__":
    app.run(debug=True)

