# StockFolio

>Exemple avec des cryptomonnaies

# Web app Flask
>Permet de visualiser son stock d'actif dans un graphique en donut.  
Base de donnée SQLite pour ajouter, modifier et supprimer ses cryptos.  
Graphique réalisé avec matplotlib...  

# Script de test
* plotlyDonut_crypto.py
>Permet d'acceder à la base de donnée SQLite et d'afficher le graphique avec plotly express.  



