import sqlite3 as sq
import datetime as dt
import investpy
import plotly.express as px

conn = sq.connect("db_portfolio.db")
c = conn.cursor()
c.execute("SELECT * FROM addcrypto")
items = c.fetchall()

print(items)

LIST_CRYPTO = []
LIST_QUANTITY = []
for item in items:
    LIST_CRYPTO.append(item[1])
    LIST_QUANTITY.append(item[2])

print(LIST_CRYPTO)
print(LIST_QUANTITY)

conn.commit()
conn.close()

prices = []
total = []

start = dt.datetime(2014, 1, 1).strftime("%d/%m/%Y")
end = dt.datetime.now().strftime("%d/%m/%Y")

for CRYPTO in LIST_CRYPTO:
    df_crypto = investpy.get_crypto_historical_data(crypto=str(CRYPTO), from_date=start, to_date=end)
    price = df_crypto[-1:]['Close'][0]
    prices.append(price)
    index = LIST_CRYPTO.index(CRYPTO)
    total.append(price * LIST_QUANTITY[index])


df_graph = px.data.tips()
fig = px.pie(df_graph, values=total, names=LIST_CRYPTO, hole=.3, color_discrete_sequence=['#de425b', '#ef805b', '#f7b672'])

fig.update_traces(
    hoverinfo='label',
    hovertemplate="<b>Crypto</b> : %{label}<br><b>Valeurs</b> : %{value}<extra></extra>"
)
fig.update_layout(
    title="StockFolio",
    legend_title="Legende",
    font=dict(
        family="Courier New, monospace",
        size=14,
        color="RebeccaPurple"
    )
)
fig.show()









