# HELLO CRYPTO

## DASHBOARD SUR LES CRYPTOMONNAIES

### UTILE
* Il est Préférable de crée un environnement virtuelles
* Installer les librairies nécessaires
* Il faut obligatoirement internet pour executer l'application
* Pour utilisé l'application il suffis de lançer le fichier main.py

* Par default l'application ce lance : http://127.0.0.1:5000/
* Si vous voulez modifier cela il faut décommenter et changer les informations de cette ligne : # host=127.0.0.1, port=5000

* La première page demande un pseudo qui sera sauvegardé durant le temps de la navigation

* Le menu à gauche se déroule quand on passe la sourie dessus (affichage non persistant) 
* Quand on click sur le cercle noir en haut à droite (affichage persistant)

* Vous pouvez clicker sur les noms des cryptomonnaies dans le tableau des cours boursier du top 10 pour afficher les pages associés
* La bar de recherche permet d'afficher les pages et les graphiques associé à la cryptomonnaie recherché
* En clickant sur les noms des cryptos dans les pages descriptives cela affiche les graphiques avec matplotlib dans une nouvelle fenetre


### SOURCES
>Le graphique générer avec plotly dash concernant la consomation éléctrique du Bitcoin à été inspirée du site 
du Cambridge [Bitcoin Electricity Consumption Index (CBECI)](https://ccaf.io/cbeci/index "CBECI")  

>Les données boursière provienne de la page [investing](https://fr.investing.com/crypto/ "investing")  


### VERSION DES MODULES ET DÉPENDANCES
python 3.8  
Flask	2.0.2  
Jinja2	3.0.2  
MarkupSafe	2.0.1  
MechanicalSoup	1.1.0  
Pillow	8.4.0  
Unidecode	1.3.2  
Werkzeug	2.0.2  
beautifulsoup4	4.10.0  
certifi	2021.10.8  
charset-normalizer	2.0.9  
click	8.0.3  
cycler	0.11.0  
fonttools	4.28.5  
idna	3.3  
investpy	1.0.7  
itsdangerous	2.0.1  
kiwisolver	1.3.2  
lxml	4.7.1  
matplotlib	3.5.1  
numpy	1.21.4  
packaging	21.3  
pandas	1.3.4  
pip	21.3.1  
plotly	5.3.1  
pyparsing	3.0.6  
python-dateutil	2.8.2  
pytz	2021.3  
requests	2.26.0  
requires.io	0.2.6  
scipy	1.8.0  
seaborn	0.11.2  
setuptools	58.5.2  
six	1.16.0  
soupsieve   2.3.1  
tenacity	8.0.1  
urllib3	1.26.7  

# ARBORESCENCE DU PROJET
HELLO CRYPTO  
  * templates  
    - bienvenu.html  
    * errors : fichier html pour les messages erreurs  
      - page_not_found.html  
    * layouts : Page principal de l'application  
      - default.html  
    * pages : Les différentes pages de l'application  
      - accueil.html  
      - aides.html  
      - cbeci.html  
      - contacts.html  
      - cours.html  
      - crypto.html  
      - deconnexion.html  
      - identification.html  
      - messages.html  
      - parametres.html  
      - profile.html  
  * partials : Parties communes à toutes les pages (ex : Bare de navigation)  
    - _nav.html  
  * static : Fichiers Statiques
    * asset : Images, icones...  
    * css : Feuille des styles css  
      - style.css  
    * font : Bibliothéque des polices, typographie...  
      - fontawesome-free-5.15.4-web  
  * main.py  
  * page_graph_crypto.py  
  * README.md  
  * __pycache__ : Dossier de cache de l'application  














