from flask import Flask, render_template, request

import investpy

from matplotlib.figure import Figure
import seaborn as sns
import plotly
import plotly.express as px

import base64
from io import BytesIO
import datetime as dt

import json

app = Flask(__name__)


start = dt.datetime(2010, 1, 1).strftime("%d/%m/%Y")
end = dt.datetime.now().strftime("%d/%m/%Y")

@app.route('/Graph_C')
def Graph_C():
    start = dt.datetime(2010, 1, 1).strftime("%d/%m/%Y")
    end = dt.datetime.now().strftime("%d/%m/%Y")
    name_crypto = request.args.get('Name')
    data_crypto = investpy.get_crypto_historical_data(crypto=str(name_crypto), from_date=start, to_date=end)
    fig = Figure(figsize=(12, 6))
    ax = fig.subplots()
    ax.plot(data_crypto['Close'], color='lightgrey')
    ax.set_title(f"Cours des clotures du {name_crypto}", fontsize=15, color='white')
    ax.set_xlabel(r"Années", fontsize=10, color='white')
    ax.set_ylabel(r"Clotures", fontsize=10, color='white')

    ax.grid(True, color='#555555')
    ax.set_axisbelow(True)
    ax.set_facecolor('black')
    ax.figure.set_facecolor('#121212')
    ax.tick_params(axis='x', colors='white')
    ax.tick_params(axis='y', colors='white')

    buf = BytesIO()
    fig.savefig(buf, format="png")
    data = base64.b64encode(buf.getbuffer()).decode("ascii")
    return f'''
    <link rel="stylesheet" type="text/css" href="static/css/style.css" />
    <img src="data:image/png;base64,{data}"/>
    '''

@app.route('/graph')
def Graph(search_crypto):
    metric = "Close"
    start = dt.datetime(2010, 1, 1).strftime("%d/%m/%Y")
    end = dt.datetime.now().strftime("%d/%m/%Y")
    colnames = []
    first = True

    data = investpy.get_crypto_historical_data(crypto=str(search_crypto), from_date=start, to_date=end)
    if first:
        combined = data[[metric]].copy()
        colnames.append(search_crypto)
        combined.columns = colnames
        first = False
    else:
        print("PAS DE GRAPHIQUE")

    fig = px.line(combined)
    fig.update_layout(xaxis_title='Années', yaxis_title='Clotures', showlegend=False)
    fig.update_xaxes(
        rangeslider_visible=True,
        rangeselector=dict(
            buttons=list([
                dict(count=1, label="1m", step="month", stepmode="backward"),
                dict(count=6, label="6m", step="month", stepmode="backward"),
                dict(count=1, label="YTD", step="year", stepmode="todate"),
                dict(count=1, label="1y", step="year", stepmode="backward"),
                dict(count=2, label="2y", step="year", stepmode="backward"),
                dict(count=3, label="3y", step="year", stepmode="backward"),
                dict(count=5, label="5y", step="year", stepmode="backward"),
                dict(step="all")
            ])
        )
    )

    graphJSON = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
    header = f"{search_crypto}"
    description = f"Court de cloture du {search_crypto}"

    return render_template('pages/crypto.html', graphJSON=graphJSON, header=header, description=description)

@app.route('/agclotures')
def agclo():
    metric = "Close"
    start = '01/01/2020'
    end = '01/01/2022'
    crypto = ['Bitcoin', 'Ethereum', 'Tether', 'binance coin', 'USD Coin', 'XRP', 'Terra', 'Cardano', 'Solana', 'Avalanche']
    colnames = []
    first = True

    for ticker in crypto:
        data = investpy.get_crypto_historical_data(crypto=str(ticker), from_date=start, to_date=end)
        if first:
            combined = data[[metric]].copy()
            colnames.append(ticker)
            combined.columns = colnames
            first = False
        else:
            combined = combined.join(data[metric])
            colnames.append(ticker)
            combined.columns = colnames

    fig = Figure(figsize=(12, 6))
    ax = fig.subplots()
    ax.set_yscale('log')
    ax.set_title("Cours des clotures des 10 cryptomonnaies", fontsize=15, color='white')
    ax.set_xlabel(r"Années", fontsize=10, color='white')
    ax.set_ylabel(r"Clotures", fontsize=10, color='white')
    for ticker in crypto:
        ax.plot(combined[ticker], label=ticker)
    ax.legend(loc="upper right")

    ax.grid(True, color='#555555')
    ax.set_axisbelow(True)
    ax.set_facecolor('black')
    ax.figure.set_facecolor('#121212')
    ax.tick_params(axis='x', colors='white')
    ax.tick_params(axis='y', colors='white')

    buf = BytesIO()
    fig.savefig(buf, format="png")
    data = base64.b64encode(buf.getbuffer()).decode("ascii")
    return f'''
        <link rel="stylesheet" type="text/css" href="static/css/style.css" />
        <img src="data:image/png;base64,{data}"/>
        '''

@app.route('/correlation')
def tcorr():
    metric = "Close"
    start = '01/01/2020'
    end = '01/01/2022'
    crypto = ['Bitcoin', 'Ethereum', 'Tether', 'binance coin', 'USD Coin', 'XRP', 'Terra', 'Cardano', 'Solana', 'Avalanche']
    colnames = []
    first = True

    for ticker in crypto:
        data = investpy.get_crypto_historical_data(crypto=str(ticker), from_date=start, to_date=end)
        if first:
            combined = data[[metric]].copy()
            colnames.append(ticker)
            combined.columns = colnames
            first = False
        else:
            combined = combined.join(data[metric])
            colnames.append(ticker)
            combined.columns = colnames

    combined = combined.pct_change().corr(method="pearson")
    fig = Figure(figsize=(12, 6))
    ax = fig.subplots()
    sns.heatmap(combined, annot=True, cmap="coolwarm", ax=ax, cbar=False)
    ax.set_title("Tableau de corrélation des 10 cryptomonnaies", fontsize=15, color='white')
    ax.set_facecolor('black')
    ax.figure.set_facecolor('#121212')
    ax.tick_params(axis='x', colors='white')
    ax.tick_params(axis='y', colors='white')
    ax.set_yticklabels(ax.get_yticklabels(), rotation=0, fontsize=8)
    buf = BytesIO()
    fig.savefig(buf, format="png")
    data = base64.b64encode(buf.getbuffer()).decode("ascii")
    return f'''
        <link rel="stylesheet" type="text/css" href="static/css/style.css" />
        <img src="data:image/png;base64,{data}"/>
        '''




if __name__ == '__main__':
    app.run(debug=True)


