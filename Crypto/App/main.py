from flask import Flask, render_template, request, make_response

import page_graph_crypto

import investpy

import mechanicalsoup
import re
import numpy

app = Flask(__name__)
app.secret_key = "cleaipxw88_CRYPTO"

# route url pour page_graph_crypto
app.add_url_rule('/Graph_C', view_func=page_graph_crypto.Graph_C)
app.add_url_rule('/agclotures', view_func=page_graph_crypto.agclo)
app.add_url_rule('/correlation', view_func=page_graph_crypto.tcorr)

@app.route('/')
def index():
    return render_template("bienvenu.html")

@app.route('/setcookie', methods=['POST', 'GET'])
def setcookie():
   if request.method == 'POST':
       user = request.form['name_input']
       resp = make_response(render_template('pages/accueil.html'))
       resp.set_cookie('userID', user)
       return resp

@app.route('/Accueil')
def page_Accueil():
    name = request.cookies.get('userID')
    return render_template('pages/accueil.html', name=name)

@app.route('/Profile')
def page_Profile():
    name = request.cookies.get('userID')
    return render_template('pages/profile.html', name=name)

@app.route('/Contacts')
def page_Contacts():
    return render_template('pages/contacts.html')

@app.route('/Messages')
def page_Messages():
    return render_template('pages/messages.html')

@app.route('/CoursFinanciers')
def page_CoursFinanciers():
    browser = mechanicalsoup.StatefulBrowser(user_agent='MechanicalSoup')
    url = "https://fr.investing.com/crypto/"
    browser.open(url)
    page = str(browser.get_current_page())

    GL = []

    # CRYPTO
    patternCRYPTO = '<td class="left bold elp name cryptoName first js-currency-name" title="(.{1,20})">'
    CRYPTO = re.findall(patternCRYPTO, page)
    print(CRYPTO)
    GL.append(CRYPTO)

    # TIKERS
    patternTIKERS = '<td class="left noWrap elp symb js-currency-symbol" title="(.{1,7})">'
    TIKERS = re.findall(patternTIKERS, page)
    print(TIKERS)
    GL.append(TIKERS)

    # COURS USD
    patternCOURSUSD = '<td class="price js-currency-price"><a class="pid-.{1,20}-last" href=".{1,50}">(.{1,10})</a></td>'
    COURSUSD = re.findall(patternCOURSUSD, page)
    print(COURSUSD)
    GL.append(COURSUSD)

    # CAP BOURSIERE
    patternCAPB = '<td class="js-market-cap" data-value=".{1,50}">\n(.{1,10})</td>'
    CAPB = re.findall(patternCAPB, page)
    print(CAPB)
    GL.append(CAPB)

    # VOL24h
    patternVOL24h = '<td class="js-24h-volume" data-value=".{1,50}">\n(.{1,10})</td>'
    VOL24h = re.findall(patternVOL24h, page)
    print(VOL24h)
    GL.append(VOL24h)

    # VOLT
    patternVOLT = '<td class="js-total-vol">(.{1,10})</td>'
    VOLT = re.findall(patternVOLT, page)
    print(VOLT)
    GL.append(VOLT)

    # VAR24
    patternVAR24 = '<td class="js-currency-change-24h .{1,6}Font pid-.{1,12}-pcp">\n(.{1,10}) </td>'
    VAR24 = re.findall(patternVAR24, page)
    print(VAR24)
    GL.append(VAR24)

    # VARJ7
    patternVARJ7 = '<td class="js-currency-change-7d .{1,6}Font">\n(.{1,10}) </td>'
    VARJ7 = re.findall(patternVARJ7, page)
    print(VARJ7)
    GL.append(VARJ7)

    GLL = numpy.transpose(GL)
    GLLL = []
    for i in GLL:
        for j in i:
            GLLL.append(j)

    L_ITEMS = ['Crypto', 'Tikers', 'Cours', 'CapB', 'Vol24h', 'VolT', 'Var24h', 'Var7j']
    boucle1 = 0
    boucle2 = 8
    x = 1
    LIST_CRYPTO = []
    while x <= 10:
        dict_boucle_list = {L_ITEMS[i]: GLLL[boucle1:boucle2][i] for i in range(len(L_ITEMS))}
        LIST_CRYPTO.append([dict_boucle_list])
        boucle1 = boucle1 + 8
        boucle2 = boucle2 + 9
        x = x + 1

    return render_template('pages/cours.html', LIST_CRYPTO=LIST_CRYPTO)

@app.route('/Crypto', methods=['GET', 'POST'])
def Crypto():
    search_crypto = request.args.get('Name')
    print(f"le nom de la crypto est : {search_crypto}")
    return page_graph_crypto.Graph(search_crypto)

@app.route('/Consomations')
def page_Consomations():
    return render_template('pages/cbeci.html')

@app.route('/Identification')
def page_Identification():
    return render_template('pages/identification.html')

@app.route('/Deconnexion')
def page_Deconnexion():
    return render_template('pages/deconnexion.html')

@app.route('/Aides')
def page_Aides():
    return render_template('pages/aides.html')

@app.route('/Parametres')
def page_Parametres():
    return render_template('pages/parametres.html')

@app.route('/search', methods=['GET', 'POST'])
def Search():
    search_crypto = request.args.get('q')
    cryptos = investpy.get_cryptos()
    name_crypto = list(cryptos['name'])

    if search_crypto in name_crypto:
        print(f"{search_crypto} existe bien")
        return page_graph_crypto.Graph(search_crypto)
    else:
        print(f"{search_crypto} existe pas")
        name = request.cookies.get('userID')
        return render_template('pages/accueil.html', name=name)

@app.errorhandler(404)
def page_not_found(error):
    return render_template('errors/page_not_found.html'), 404

if __name__ == '__main__':
    app.run(debug=True)
    # host=127.0.0.1, port=5000


