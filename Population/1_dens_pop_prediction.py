import pandas as pd
from scipy import stats

df = pd.read_csv("data/insee_dens_pop_35.csv")

def predict(x):
    return slope * x + intercept

annees_a_predire = [2023, 2028, 2033]

grouped = df.groupby(['codgeo', 'libgeo'])

for (codgeo, libgeo), group_data in grouped:
    X = group_data['an']
    Y = group_data['dens_pop']

    slope, intercept, r_value, p_value, std_err = stats.linregress(X, Y)

    for annee in annees_a_predire:
        prediction = round(predict(annee), 2)
        group_data = pd.concat(
            [group_data, pd.DataFrame([{'codgeo': codgeo, 'libgeo': libgeo, 'an': annee, 'dens_pop': prediction}])],
            ignore_index=True)

    df = df[df['codgeo'] != codgeo]
    df = pd.concat([df, group_data], ignore_index=True)

df.to_csv('data/dens_pop_resultats_predictions.csv', index=False)

