import folium
import pandas as pd

data = pd.read_csv('data/dens_pop_resultat_final.csv')

LIST_ANNEE = [1968, 1975, 1982, 1990, 1999, 2008, 2013, 2018, 2023, 2028, 2033]

for year in LIST_ANNEE:
    data_year = data[data['an'] == year]

    ma_carte = folium.Map(location=[48.1119791219, -1.68186449144], zoom_start=9)

    for index, row in data_year.iterrows():
        latitude, longitude = map(float, row['geometry'].strip('POINT ()').split())

        cercle = folium.Circle(
            location=(latitude, longitude),
            popup=f"Ville : {row['libgeo']}, Année : {row['an']}, Densité de population : {row['dens_pop']}",
            radius=row['dens_pop'] * 1,
            color='blue',
            fill=True,
            fill_opacity=0.3,
            weight=0.4
        )
        cercle.add_to(ma_carte)

    ma_carte.save(f'site/carte_{year}.html')
