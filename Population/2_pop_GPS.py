import pandas as pd

df_1 = pd.read_csv("data/dens_pop_resultats_predictions.csv")
df_2 = pd.read_csv("data/communes-departement-35.csv")

code_insee_to_coordinates = {}
for index, row in df_2.iterrows():
    code_insee_to_coordinates[row['code_commune_INSEE']] = (row['latitude'], row['longitude'])

def create_geometry(row):
    code_insee = row['codgeo']
    if code_insee in code_insee_to_coordinates:
        latitude, longitude = code_insee_to_coordinates[code_insee]
        return f"POINT ({latitude} {longitude})"
    else:
        return None

df_1['geometry'] = df_1.apply(create_geometry, axis=1)

df_1.to_csv("data/dens_pop_resultat_final.csv", index=False)


