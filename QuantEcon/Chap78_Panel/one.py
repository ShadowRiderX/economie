import pandas as pd

realwage = pd.read_csv('data/wage.csv', sep="|")
realwage = realwage.pivot_table(values='Value',
                                index='Time',
                                columns=['Country', 'Series', 'Pay period'])
realwage_f = realwage.xs(('Hourly', 'In 2020 constant prices at 2020 USD exchange rates'),
                         level=('Pay period', 'Series'), axis=1)
realwage_t = realwage_f.transpose().head()
print(realwage_t)

print("\n##################################################################\n")

worlddata = pd.read_csv('data/world.csv', sep=";")
worlddata = worlddata[['Country (en)', 'Continent']]
worlddata = worlddata.rename(columns={'Country (en)': 'Country'})
print(worlddata.head())

print("\n##################################################################\n")

merged = pd.merge(realwage_f.transpose(), worlddata,
                  how='left', left_index=True, right_on='Country')
print(merged.head())

'''print(merged.index)'''

print("\n##################################################################\n")

data_nan = merged[merged['Continent'].isnull()]
print(data_nan)

missing_continents = {'Korea': 'Asia',
                      'Russian Federation': 'Europe',
                      'Slovak Republic': 'Europe',
                      'Türkiye': 'Europe'}

merged['Country'].map(missing_continents)

merged['Continent'] = merged['Continent'].fillna(merged['Country'].map(missing_continents))

check_korea = merged[merged['Country'] == 'Korea']
print(check_korea)

check_australia = merged[merged['Country'] == 'Australia']
print(check_australia)

print("\n##################################################################\n")

'''detail_America = ['Central America', 'North America', 'South America']

for country in detail_America:
    merged['Continent'].replace(to_replace=country,
                                value='America',
                                inplace=True)'''

print("\n##################################################################\n")

merged = merged.set_index(['Continent', 'Country']).sort_index()
print(merged)

print("\n##################################################################\n")

'''format_date = "%d/%m/%Y - %H:%M:%S"'''
merged.columns = pd.to_datetime(merged.columns, format="%Y")
merged.columns = merged.columns.rename('Time')
merged = merged.transpose()
print(merged.head())

'''merged.to_csv("tab_final.csv")'''

print("\n##################################################################\n")

minA_wage = merged.mean()
print(minA_wage)

print("\n##################################################################\n")

import matplotlib.pyplot as plt
plt.rcParams["figure.figsize"] = (11, 5)

'''import matplotlib'''
'''matplotlib.style.use('seaborn')'''

merged.mean().sort_values(ascending=False).plot(kind='bar', title="Average real minimum wage 2001 - 2020")

country_labels = merged.mean().sort_values(ascending=False).index.get_level_values('Country').tolist()
plt.xticks(range(0, len(country_labels)), country_labels)
plt.xlabel('Country')

plt.show()

print("\n##################################################################\n")

print(merged.mean(axis=1).head())

merged.mean(axis=1).plot()
plt.title('Average real minimum wage 2001 - 2020')
plt.ylabel('2015 USD')
plt.xlabel('Year')
plt.show()

print("\n##################################################################\n")

'''print(merged.mean(level='Continent', axis=1))'''
print(merged.groupby(level=0).mean())

merged.groupby(level='Continent', axis=1).mean().plot()
plt.title('Average real minimum wage')
plt.ylabel('2015 USD')
plt.xlabel('Year')
plt.show()

print("\n##################################################################\n")

merged_stat = merged.stack().describe()
print(merged_stat)

print("\n##################################################################\n")

grouped = merged.groupby(level='Continent', axis=1)
print(grouped.size())

grouped_stat = grouped.describe()
print(grouped_stat.stack())
print(grouped_stat)

mean_Israel = grouped_stat['mean']['Asia']['Israel']
print(mean_Israel)

mean_Israel_stack = grouped_stat.stack()['Asia']['Israel']['mean']
print(mean_Israel_stack)

print("\n##################################################################\n")

import seaborn as sns

continents = grouped.groups.keys()

for continent in continents:
    '''sns.kdeplot(grouped.get_group(continent)['2020'].unstack(), label=continent, shade=True)'''
    sns.kdeplot(grouped.get_group(continent).loc['2020'].unstack(), label=continent, shade=True, warn_singular=False)

plt.title('Real minimum wages in 2020')
plt.xlabel('US dollars')
plt.legend()
plt.show()







