# Étude sur les salaires moyens dans le monde
## Application en données de Panel
### Pour la période de 2001 à 2020


import pandas as pd
import plotly.express as px
import seaborn as sns

## Donnée sur le salaire
realwage = pd.read_csv('data/wage.csv', sep="|")
print(realwage)

realwage = realwage.pivot_table(values='Value',
                                index='Time',
                                columns=['Country', 'Series', 'Pay period'])
realwage_f = realwage.xs(('Hourly', 'In 2020 constant prices at 2020 USD exchange rates'),
                         level=('Pay period', 'Series'), axis=1)
realwage_t = realwage_f.transpose().head()
print(realwage_t)


## Données des pays
worlddata = pd.read_csv('data/world.csv', sep=";")
worlddata = worlddata[['Country (en)', 'Continent']]
worlddata = worlddata.rename(columns={'Country (en)': 'Country'})
print(worlddata.head())

## Fusion des deux bases de données
merged = pd.merge(realwage_f.transpose(), worlddata,
                  how='left', left_index=True, right_on='Country')
print(merged.head())

## Traitement des données NaN
data_nan = merged[merged['Continent'].isnull()]
print(data_nan)

missing_continents = {'Korea': 'Asia',
                      'Russian Federation': 'Europe',
                      'Slovak Republic': 'Europe',
                      'Türkiye': 'Europe'}

merged['Country'].map(missing_continents)

merged['Continent'] = merged['Continent'].fillna(merged['Country'].map(missing_continents))

print(merged)



## Moyenne des salaires minimum par pays
### Pour la période de 2001 à 2020
merged_2 = merged.drop(['Continent'], axis=1)
merged_2 = merged_2.set_index(['Country']).sort_index()
merged_2.columns = merged_2.columns.rename('Time')
merged_2 = merged_2.transpose()
minA_wage_1 = merged_2.mean()
print(minA_wage_1)

df_1 = merged_2.mean(axis=1)
fig = px.bar(df_1, x=minA_wage_1.index, y=minA_wage_1).update_xaxes(categoryorder="total descending")
fig.update_layout(
    title="Moyenne des salaires minimum par pays de 2001 à 2020",
    xaxis_title="Pays",
    yaxis_title="Dollard USD - 2020",
    legend_title="Legende"
)
fig.show()



# Évolution Moyenne des salaires minimum réel
## Pour la période de 2001 à 2020
merged_3 = merged.set_index(['Continent', 'Country']).sort_index()
merged_3.columns = pd.to_datetime(merged_3.columns, format="%Y")
merged_3.columns = merged_3.columns.rename('Time')
merged_3 = merged_3.transpose()
print(merged_3.head())

df_2 = merged_3.mean(axis=1)
print(df_2.head())

fig = px.line(df_2)
fig.update_layout(
    title="Évolution Moyenne des salaires minimum réel de 2001 à 2020",
    xaxis_title="Années - 2001 à 2020",
    yaxis_title="Dollard USD - 2020",
    legend_title="Legende"
)
fig.show()



# Évolution des salaires minimum moyenne réel par région du monde
## Pour la période de 2001 à 2020 ( en USD 2020)
df_3 = merged_3.groupby(level='Continent', axis=1).mean()
print(df_3.head())

fig = px.line(df_3)
fig.update_layout(
    xaxis_title="Années - 2001 à 2020",
    yaxis_title="Dollard USD - 2020",
    title="Évolution des salaires minimum moyenne réel par région du monde",
    legend_title="Legende"
)
fig.show()

## Satistique descriptive
import matplotlib as plt
merged_stat = merged.stack().describe()
print(merged_stat)

grouped = merged.groupby(level='Continent', axis=1)
print(grouped.size())

grouped_stat = grouped.describe()
print(grouped_stat.stack())
print(grouped_stat)

continents = grouped.groups.keys()

for continent in continents:
    sns.kdeplot(grouped.get_group(continent).loc['2020'].unstack(), label=continent, shade=True, warn_singular=False)

plt.title('Salaire réel minimum en 2020')
plt.xlabel('US dollars')
plt.legend()
plt.show()

# Fin de l'étude


